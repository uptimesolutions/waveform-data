/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.waveform.utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.uptime.cassandra.trend.entity.Waveform;
import com.uptime.waveform.vo.WaveFormDataVO;
import static com.uptime.services.util.HttpUtils.parseInstant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author Mohd Juned Alam
 */
public class JsonUtil {

    /**
     * Parse the given json String Object and return a WaveFormDataVO object
     *
     * @param content, String Object
     * @return WaveFormDataVO Object
     */
    public static WaveFormDataVO parser(String content) {
        WaveFormDataVO waveFormDataVO = null;
        JsonElement element;
        JsonObject jsonObject;
        JsonArray jsonArray;
        Waveform waveform;

        if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null && (jsonObject = element.getAsJsonObject()) != null) {
            waveFormDataVO = new WaveFormDataVO();
            waveFormDataVO.setWaveform(new Waveform());

            if (jsonObject.has("customerAccount")) {
                try {
                    waveFormDataVO.setCustomerAccount(jsonObject.get("customerAccount").getAsString());
                } catch (Exception ex) {
                }
            }

            if (jsonObject.has("siteId")) {
                try {
                    waveFormDataVO.setSiteId(UUID.fromString(jsonObject.get("siteId").getAsString()));
                } catch (Exception ex) {
                }
            }

            if (jsonObject.has("areaId")) {
                try {
                    waveFormDataVO.setAreaId(UUID.fromString(jsonObject.get("areaId").getAsString()));
                } catch (Exception ex) {
                }
            }

            if (jsonObject.has("assetId")) {
                try {
                    waveFormDataVO.setAssetId(UUID.fromString(jsonObject.get("assetId").getAsString()));
                } catch (Exception ex) {
                }
            }

            if (jsonObject.has("pointLocationId")) {
                try {
                    waveFormDataVO.setPointLocationId(UUID.fromString(jsonObject.get("pointLocationId").getAsString()));
                } catch (Exception ex) {
                }
            }

            if (jsonObject.has("pointId")) {
                try {
                    waveFormDataVO.setPointId(UUID.fromString(jsonObject.get("pointId").getAsString()));
                } catch (Exception ex) {
                }
            }

            if (jsonObject.has("apSetId")) {
                try {
                    waveFormDataVO.setApSetId(UUID.fromString(jsonObject.get("apSetId").getAsString()));
                } catch (Exception ex) {
                }
            }

            if (jsonObject.has("alSetId")) {
                try {
                    waveFormDataVO.setAlSetId(UUID.fromString(jsonObject.get("alSetId").getAsString()));
                } catch (Exception ex) {
                }
            }

            if (jsonObject.has("paramName")) {
                try {
                    waveFormDataVO.setParamName(jsonObject.get("paramName").getAsString());
                } catch (Exception ex) {
                }
            }

            if (jsonObject.has("sampleTime")) {
                try {
                    waveFormDataVO.setSampleTime(parseInstant(jsonObject.get("sampleTime").getAsString()));
                    waveFormDataVO.getWaveform().setSampleTime(waveFormDataVO.getSampleTime());
                } catch (Exception ex) {
                    System.out.println("sampleTime exception - " + ex.toString());
                }
            }

            if (jsonObject.has("value")) {
                try {
                    waveFormDataVO.setValue(jsonObject.get("value").getAsFloat());
                } catch (Exception ex) {
                }
            }

            if (jsonObject.has("highAlert")) {
                try {
                    waveFormDataVO.setHighAlert(jsonObject.get("highAlert").getAsFloat());
                } catch (Exception ex) {
                }
            }

            if (jsonObject.has("highFault")) {
                try {
                    waveFormDataVO.setHighFault(jsonObject.get("highFault").getAsFloat());
                } catch (Exception ex) {
                }
            }

            if (jsonObject.has("lowAlert")) {
                try {
                    waveFormDataVO.setLowAlert(jsonObject.get("lowAlert").getAsFloat());
                } catch (Exception ex) {
                }
            }

            if (jsonObject.has("lowFault")) {
                try {
                    waveFormDataVO.setLowFault(jsonObject.get("lowFault").getAsFloat());
                } catch (Exception ex) {
                }
            }

            if (jsonObject.has("label")) {
                try {
                    waveFormDataVO.setLabel(jsonObject.get("label").getAsString());
                } catch (Exception ex) {
                }
            }

            if (jsonObject.has("notes")) {
                try {
                    waveFormDataVO.setNotes(jsonObject.get("notes").getAsString());
                } catch (Exception ex) {
                }
            }

            if (jsonObject.has("units")) {
                try {
                    waveFormDataVO.setUnits(jsonObject.get("units").getAsString());
                } catch (Exception ex) {
                }
            }

            if (jsonObject.has("sensorType")) {
                try {
                    waveFormDataVO.setSensorType(jsonObject.get("sensorType").getAsString());
                } catch (Exception ex) {
                }
            }

            if (jsonObject.has("sampleMonth")) {
                try {
                    waveFormDataVO.setSampleMonth(jsonObject.get("sampleMonth").getAsByte());
                } catch (Exception ex) {
                }
            }

            if (jsonObject.has("sampleYear")) {
                try {
                    waveFormDataVO.setSampleYear(jsonObject.get("sampleYear").getAsShort());
                } catch (Exception ex) {
                }
            }

            if (jsonObject.has("sensitivityValue")) {
                try {
                    waveFormDataVO.setSensitivityValue(jsonObject.get("sensitivityValue").getAsFloat());
                } catch (Exception ex) {
                }
            }

            if (jsonObject.has("deviceId")) {
                try {
                    waveFormDataVO.getWaveform().setDeviceId(jsonObject.get("deviceId").getAsString());
                } catch (Exception ex) {
                }
            }

            if (jsonObject.has("channelNum")) {
                try {
                    waveFormDataVO.getWaveform().setChannelNum(jsonObject.get("channelNum").getAsByte());
                } catch (Exception ex) {
                }
            }

            if (jsonObject.has("sampleRateInHz")) {
                try {
                    waveFormDataVO.getWaveform().setSampleRateHz(jsonObject.get("sampleRateInHz").getAsFloat());
                } catch (Exception ex) {
                }
            }

            if (jsonObject.has("sensitivityUnits")) {
                try {
                    waveFormDataVO.getWaveform().setSensitivityUnits(jsonObject.get("sensitivityUnits").getAsString());
                } catch (Exception ex) {
                }
            }

            if (jsonObject.has("tachSpeed")) {
                try {
                    waveFormDataVO.getWaveform().setTachSpeed(jsonObject.get("tachSpeed").getAsFloat());
                } catch (Exception ex) {
                }
            }

            if (jsonObject.has("waveData")) {
                if ((jsonArray = jsonObject.get("waveData").getAsJsonArray()) != null) {
                    List<Double> sampleList = new ArrayList();

                    jsonArray.forEach(ele -> {
                        try {
                            sampleList.add(ele.getAsDouble());
                        } catch (Exception ex) {
                            System.out.println("waveData Exception - " + ex.toString());
                        }
                    });

                    waveFormDataVO.getWaveform().setSamples(sampleList);
                }
            }

            if (jsonObject.has("waveform")) {
                if ((jsonObject = jsonObject.get("waveform").getAsJsonObject()) != null) {
                    waveform = new Waveform();

                    if (jsonObject.has("sampleTime")) {
                        try {
                            waveform.setSampleTime(parseInstant(jsonObject.get("sampleTime").getAsString()));
                        } catch (Exception ex) {
                        }
                    }

                    if (jsonObject.has("deviceId")) {
                        try {
                            waveform.setDeviceId(jsonObject.get("deviceId").getAsString());
                        } catch (Exception ex) {
                        }
                    }

                    if (jsonObject.has("channelNum")) {
                        try {
                            waveform.setChannelNum(jsonObject.get("channelNum").getAsByte());
                        } catch (Exception ex) {
                        }
                    }

                    if (jsonObject.has("sampleRateHz")) {
                        try {
                            waveform.setSampleRateHz(jsonObject.get("sampleRateHz").getAsFloat());
                        } catch (Exception ex) {
                        }
                    }

                    if (jsonObject.has("sensitivityUnits")) {
                        try {
                            waveform.setSensitivityUnits(jsonObject.get("sensitivityUnits").getAsString());
                        } catch (Exception ex) {
                        }
                    }

                    if (jsonObject.has("tachSpeed")) {
                        try {
                            waveform.setTachSpeed(jsonObject.get("tachSpeed").getAsFloat());
                        } catch (Exception ex) {
                        }
                    }

                    if (jsonObject.has("samples")) {
                        if ((jsonArray = jsonObject.get("samples").getAsJsonArray()) != null) {
                            List<Double> sampleList = new ArrayList<>();

                            jsonArray.forEach(element1 -> {
                                try {
                                    sampleList.add(element1.getAsDouble());
                                } catch (Exception ex) {
                                    System.out.println("samples Exception - " + ex.toString());
                                }
                            });
                            waveform.setSamples(sampleList);
                        }
                    }
                    waveFormDataVO.setWaveform(waveform);
                }
            }
        }

        return waveFormDataVO;
    }
}
