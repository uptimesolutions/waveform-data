/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.waveform.utils;

import com.uptime.cassandra.trend.entity.TrendAcValues;
import com.uptime.cassandra.trend.entity.TrendDcValues;
import com.uptime.cassandra.trend.entity.WaveData;
import com.uptime.waveform.vo.WaveFormDataVO;

/**
 *
 * @author Mohd Juned Alam
 */
public class DelegateUtil {

    /**
     * Convert WaveFormDataVO Object to TrendAcValues Object
     *
     * @param waveFormDataVO, WaveFormDataVO Object
     * @return TrendAcValues Object
     */
    public static TrendAcValues getTrendAcValues(WaveFormDataVO waveFormDataVO) {
        TrendAcValues entity = new TrendAcValues();

        if (waveFormDataVO != null) {
            entity.setAlSetId(waveFormDataVO.getAlSetId());
            entity.setApSetId(waveFormDataVO.getApSetId());
            entity.setAreaId(waveFormDataVO.getAreaId());
            entity.setAssetId(waveFormDataVO.getAssetId());
            entity.setCustomerAccount(waveFormDataVO.getCustomerAccount());
            entity.setParamName(waveFormDataVO.getParamName());
            entity.setPointId(waveFormDataVO.getPointId());
            entity.setPointLocationId(waveFormDataVO.getPointLocationId());
            entity.setSampleTime(waveFormDataVO.getSampleTime());
            entity.setSiteId(waveFormDataVO.getSiteId());
            entity.setLabel(waveFormDataVO.getLabel());
            entity.setNotes(waveFormDataVO.getNotes());
            entity.setUnits(waveFormDataVO.getUnits());
            entity.setValue(waveFormDataVO.getValue());
            entity.setHighAlert(waveFormDataVO.getHighAlert());
            entity.setHighFault(waveFormDataVO.getHighFault());
            entity.setLowAlert(waveFormDataVO.getLowAlert());
            entity.setLowFault(waveFormDataVO.getLowFault());
            entity.setSensorType(waveFormDataVO.getSensorType());
        }
        return entity;
    }

    /**
     * Convert WaveFormDataVO Object to TrendDcValues Object
     *
     * @param waveFormDataVO, WaveFormDataVO Object
     * @return TrendDcValues Object
     */
    public static TrendDcValues getTrendDcValues(WaveFormDataVO waveFormDataVO) {
        TrendDcValues entity = new TrendDcValues();
        
        if (waveFormDataVO != null) {
            entity.setAlSetId(waveFormDataVO.getAlSetId());
            entity.setApSetId(waveFormDataVO.getApSetId());
            entity.setAreaId(waveFormDataVO.getAreaId());
            entity.setAssetId(waveFormDataVO.getAssetId());
            entity.setCustomerAccount(waveFormDataVO.getCustomerAccount());
            entity.setPointId(waveFormDataVO.getPointId());
            entity.setPointLocationId(waveFormDataVO.getPointLocationId());
            entity.setSampleTime(waveFormDataVO.getSampleTime());
            entity.setSiteId(waveFormDataVO.getSiteId());
            entity.setLabel(waveFormDataVO.getLabel());
            entity.setUnits(waveFormDataVO.getUnits());
            entity.setNotes(waveFormDataVO.getNotes());
            entity.setValue(waveFormDataVO.getValue());
            entity.setHighAlert(waveFormDataVO.getHighAlert());
            entity.setHighFault(waveFormDataVO.getHighFault());
            entity.setLowAlert(waveFormDataVO.getLowAlert());
            entity.setLowFault(waveFormDataVO.getLowFault());
            entity.setSensorType(waveFormDataVO.getSensorType());
        }
        return entity;
    }
    
    /**
     * Convert WaveFormDataVO Object to TrendDcValues Object
     *
     * @param waveFormDataVO, WaveFormDataVO Object
     * @return TrendDcValues Object
     */
    public static WaveData getWaveData(WaveFormDataVO waveFormDataVO) {
        WaveData entity = new WaveData();
        
        if (waveFormDataVO != null) {
            entity.setCustomerAccount(waveFormDataVO.getCustomerAccount());
            entity.setSiteId(waveFormDataVO.getSiteId());
            entity.setAreaId(waveFormDataVO.getAreaId());
            entity.setAssetId(waveFormDataVO.getAssetId());
            entity.setPointLocationId(waveFormDataVO.getPointLocationId());
            entity.setPointId(waveFormDataVO.getPointId());
            entity.setSampleYear(waveFormDataVO.getSampleYear());
            entity.setSampleMonth(waveFormDataVO.getSampleMonth());
            entity.setSampleTime(waveFormDataVO.getSampleTime());
            entity.setSensitivityValue(waveFormDataVO.getSensitivityValue());
            entity.setWave(waveFormDataVO.getWaveform());
        }
        return entity;
    }
}
