/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.waveform.http.handlers;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.uptime.services.http.handler.AbstractGenericHandlerNew;
import static com.uptime.services.util.HttpUtils.parseQuery;
import static com.uptime.services.util.ServiceUtil.streamReader;
import static com.uptime.waveform.WaveformDataService.running;
import static com.uptime.waveform.WaveformDataService.sendEvent;
import com.uptime.waveform.controller.TrendController;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class TrendHandler extends AbstractGenericHandlerNew {
    private static final Logger LOGGER = LoggerFactory.getLogger(TrendHandler.class.getName());
    TrendController controller;

    public TrendHandler() {
        controller = new TrendController();
    }

    /**
     * HTTP GET handler
     *
     * @param he
     * @throws IOException
     */
    @Override
    public void doGet(HttpExchange he) throws IOException {
        StackTraceElement[] stackTrace = null;
        String resp;
        Map<String, Object> params;
        byte[] response;
        OutputStream os;
        Headers respHeaders;

        try {
            if ((params = parseQuery(he.getRequestURI().getRawQuery())).isEmpty()) {
                resp = "{\"outcome\":\"Hello World!\"}";
            } else if (params.containsKey("customerAccount") && params.containsKey("siteId") && params.containsKey("areaId") && params.containsKey("assetId") && params.containsKey("pointLocationId") && params.containsKey("pointId") && params.containsKey("apSetId") && params.containsKey("paramId") && params.containsKey("startDate") && params.containsKey("endDate")) {
                try {
                    LOGGER.info("customerAccount: {}, siteId: {}, areaId: {}, assetId: {}, pointLocationId: {}, pointId : {}, apSetId: {}, paramId: {}, startDate : {}, endDate: {}", new Object[]{params.get("customerAccount").toString(), params.get("siteId").toString(), params.get("areaId").toString(), params.get("assetId").toString(), params.get("pointLocationId").toString(), params.get("pointId").toString(), params.get("apSetId").toString(), params.get("paramId").toString(), params.get("startDate").toString(), params.get("endDate").toString()});
                    resp = controller.getByCustomerSiteAreaAssetPointLocationPointApSetParamDateRange(params.get("customerAccount").toString(), params.get("siteId").toString(), params.get("areaId").toString(), params.get("assetId").toString(), params.get("pointLocationId").toString(), params.get("pointId").toString(), params.get("apSetId").toString(), params.get("paramId").toString(), params.get("startDate").toString(), params.get("endDate").toString());
                } catch (NumberFormatException | NullPointerException e) {
                    resp = "{\"outcome\":\"A valid customerAccount, siteId, areaId, assetId, pointLocationId, pointId, apSetId, paramId and sampleTime are required.\"}";
                }
            } else if (params.containsKey("customerAccount") && params.containsKey("siteId") && params.containsKey("areaId") && params.containsKey("assetId") && params.containsKey("pointLocationId") && params.containsKey("pointId") && params.containsKey("apSetId") && params.containsKey("startDate") && params.containsKey("endDate")) {
                try {
                    LOGGER.info("customerAccount: {}, siteId: {}, areaId: {}, assetId: {}, pointLocationId: {}, pointId : {}, apSetId: {}, startDate : {}, endDate: {}", new Object[]{params.get("customerAccount").toString(), params.get("siteId").toString(), params.get("areaId").toString(), params.get("assetId").toString(), params.get("pointLocationId").toString(), params.get("pointId").toString(), params.get("apSetId").toString(), params.get("startDate").toString(), params.get("endDate").toString()});
                    resp = controller.getByCustomerSiteAreaAssetPointLocationPointApSetDateRange(params.get("customerAccount").toString(), params.get("siteId").toString(), params.get("areaId").toString(), params.get("assetId").toString(), params.get("pointLocationId").toString(), params.get("pointId").toString(), params.get("apSetId").toString(), params.get("startDate").toString(), params.get("endDate").toString());
                } catch (NumberFormatException | NullPointerException e) {
                    resp = "{\"outcome\":\"A valid customerAccount, siteId, areaId, assetId, pointLocationId, pointId, apSetId, paramId and sampleTime are required.\"}";
                }
            } else if (params.containsKey("customerAccount") && params.containsKey("siteId") && params.containsKey("areaId") && params.containsKey("assetId") && params.containsKey("pointLocationId") && params.containsKey("pointId") && params.containsKey("apSetId") && params.containsKey("paramId") && params.containsKey("sampleTime")) {
                try {
                    LOGGER.info("customerAccount: {}, siteId: {}, areaId: {}, assetId: {}, pointLocationId: {}, pointId : {}, apSetId: {}, paramId: {}, rowId : {}", new Object[]{params.get("customerAccount").toString(), params.get("siteId").toString(), params.get("areaId").toString(), params.get("assetId").toString(), params.get("pointLocationId").toString(), params.get("pointId").toString(), params.get("apSetId").toString(), params.get("paramId").toString(), params.get("sampleTime").toString()});
                    resp = controller.getByCustomerSiteAreaAssetPointLocationPointApSetParamSampleTime(params.get("customerAccount").toString(), params.get("siteId").toString(), params.get("areaId").toString(), params.get("assetId").toString(), params.get("pointLocationId").toString(), params.get("pointId").toString(), params.get("apSetId").toString(), params.get("paramId").toString(), params.get("sampleTime").toString());
                } catch (NumberFormatException | NullPointerException e) {
                    resp = "{\"outcome\":\"A valid customerAccount, siteId, areaId, assetId, pointLocationId, pointId, apSetId, paramId and sampleTime are required.\"}";
                }
            } else if (params.containsKey("customerAccount") && params.containsKey("siteId") && params.containsKey("areaId") && params.containsKey("assetId") && params.containsKey("pointLocationId") && params.containsKey("pointId") && params.containsKey("apSetId") && params.containsKey("paramId") && params.containsKey("limit")) {
                try {
                    LOGGER.info("customerAccount: {}, siteId: {}, areaId: {}, assetId: {}, pointLocationId: {}, pointId : {}, apSetId: {}, paramId: {}, limit: {}", new Object[]{params.get("customerAccount").toString(), params.get("siteId").toString(), params.get("areaId").toString(), params.get("assetId").toString(), params.get("pointLocationId").toString(), params.get("pointId").toString(), params.get("apSetId").toString(), params.get("paramId").toString(), params.get("limit").toString()});
                    resp = controller.getByCustomerSiteAreaAssetPointLocationPointApSetParamLimit(params.get("customerAccount").toString(), params.get("siteId").toString(), params.get("areaId").toString(), params.get("assetId").toString(), params.get("pointLocationId").toString(), params.get("pointId").toString(), params.get("apSetId").toString(), params.get("paramId").toString(), params.get("limit").toString());
                } catch (NumberFormatException | NullPointerException e) {
                    resp = "{\"outcome\":\"A valid customerAccount, siteId, areaId, assetId, pointLocationId, pointId, apSetId, paramId and limit are required.\"}";
                }
            } else if (params.containsKey("customerAccount") && params.containsKey("siteId") && params.containsKey("areaId") && params.containsKey("assetId") && params.containsKey("pointLocationId") && params.containsKey("pointId") && params.containsKey("apSetId") && params.containsKey("sampleTime")) {
                try {
                    LOGGER.info("customerAccount: {}, siteId: {}, areaId: {}, assetId: {}, pointLocationId: {}, pointId : {}, apSetId: {}, sampleTime: {}", new Object[]{params.get("customerAccount").toString(), params.get("siteId").toString(), params.get("areaId").toString(), params.get("assetId").toString(), params.get("pointLocationId").toString(), params.get("pointId").toString(), params.get("apSetId").toString(), params.get("sampleTime").toString()});
                    resp = controller.getByCustomerSiteAreaAssetPointLocationPointApSetSampleTime(params.get("customerAccount").toString(), params.get("siteId").toString(), params.get("areaId").toString(), params.get("assetId").toString(), params.get("pointLocationId").toString(), params.get("pointId").toString(), params.get("apSetId").toString(), params.get("sampleTime").toString());
                } catch (NumberFormatException | NullPointerException e) {
                    resp = "{\"outcome\":\"A valid customerAccount, siteId, areaId, assetId, pointLocationId, pointId, apSetId and sampleTime are required.\"}";
                }
            } else if (params.containsKey("customerAccount") && params.containsKey("siteId") && params.containsKey("areaId") && params.containsKey("assetId") && params.containsKey("pointLocationId") && params.containsKey("pointId") && params.containsKey("apSetId") && params.containsKey("limit")) {
                try {
                    LOGGER.info("customerAccount: {}, siteId: {}, areaId: {}, assetId: {}, pointLocationId: {}, pointId : {}, apSetId: {}, limit: {}", new Object[]{params.get("customerAccount").toString(), params.get("siteId").toString(), params.get("areaId").toString(), params.get("assetId").toString(), params.get("pointLocationId").toString(), params.get("pointId").toString(), params.get("apSetId").toString(), params.get("limit").toString()});
                    resp = controller.getByCustomerSiteAreaAssetPointLocationPointApSetLimit(params.get("customerAccount").toString(), params.get("siteId").toString(), params.get("areaId").toString(), params.get("assetId").toString(), params.get("pointLocationId").toString(), params.get("pointId").toString(), params.get("apSetId").toString(), params.get("limit").toString());
                } catch (NumberFormatException | NullPointerException e) {
                    resp = "{\"outcome\":\"A valid customerAccount, siteId, areaId, assetId, pointLocationId, pointId, apSetId and limit are required.\"}";
                }
            } else {
                resp = "{\"outcome\":\"Unknown Params\"}";
            }

            response = resp.getBytes();
            if (resp.contains("Unknown Params") || (resp.contains("A valid") && resp.contains("required."))) {
                he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, response.length);
            } else if (resp.contains("Error: ")) {
                he.sendResponseHeaders(HttpURLConnection.HTTP_UNAVAILABLE, response.length);
            } else if (resp.contains("Null value received from database.")) {
                he.sendResponseHeaders(HttpURLConnection.HTTP_NOT_FOUND, response.length);
            } else {
                he.sendResponseHeaders(HttpURLConnection.HTTP_OK, response.length);
            }
        } catch (IllegalArgumentException e) {
            LOGGER.warn(e.getMessage(), e);
            response = "{\"outcome\":\"IllegalArgumentException\"}".getBytes();
            he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, response.length);
        } catch (ReadTimeoutException e) {
            stackTrace = setStackTrace(e, (response = "{\"outcome\":\"ReadTimeoutException\"}".getBytes()), he);
        } catch (UnavailableException e) {
            stackTrace = setStackTrace(e, (response = "{\"outcome\":\"UnavailableException\"}".getBytes()), he);
        } catch (UnsupportedEncodingException e) {
            stackTrace = setStackTrace(e, (response = "{\"outcome\":\"UnsupportedEncodingException\"}".getBytes()), he);
        } catch (Exception e) {
            stackTrace = setStackTrace(e, (response = "{\"outcome\":\"Exception\"}".getBytes()), he);
        }

        respHeaders = he.getResponseHeaders();
        respHeaders.set("Content-Type", "application/json");
        os = he.getResponseBody();
        os.write(response);
        os.flush();
        he.close();

        // Send Event
        if (stackTrace != null) {
            sendEvent(stackTrace);
        }
    }

    /**
     *
     * @param he
     * @throws IOException
     */
    @Override
    public void doPost(HttpExchange he) throws IOException {
        StackTraceElement[] stackTrace = null;
        byte[] response;
        OutputStream os;
        Headers respHeaders;
        String resp;
        String content;

        try {
            if (he.getRequestBody() == null) {
                resp = "{\"outcome\":\"RequestBody is invalid\"}";
            } else {
                if ((content = streamReader(he.getRequestBody())) != null) {
                    LOGGER.info("*********JSON VALUE:{}", content);
                    resp = controller.createTrendValues(content);
                } else {
                    resp = "{\"outcome\":\"RequestBody is invalid\"}";
                }
            }
            response = resp.getBytes();
            if (resp.contains(" successfully.")) {
                he.sendResponseHeaders(HttpURLConnection.HTTP_OK, response.length);
            } else if (resp.contains("Error: ")) {
                he.sendResponseHeaders(HttpURLConnection.HTTP_UNAVAILABLE, response.length);
            } else {
                he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, response.length);
            }
        } catch (Exception e) {
            stackTrace = setStackTrace(e, (response = "{\"outcome\":\"Exception\"}".getBytes()), he);
        }

        respHeaders = he.getResponseHeaders();
        respHeaders.set("Content-Type", "application/json");
        os = he.getResponseBody();
        os.write(response);
        os.flush();
        he.close();

        if (stackTrace != null) {
            sendEvent(stackTrace);
        }
    }

    /**
     * HTTP PUT handler
     *
     * @param he
     * @throws IOException
     */
    @Override
    public void doPut(HttpExchange he) throws IOException {
        StackTraceElement[] stackTrace = null;
        byte[] response;
        OutputStream os;
        Headers respHeaders;
        String resp;
        String content;

        try {
            if (he.getRequestBody() == null) {
                resp = "{\"outcome\":\"RequestBody is invalid\"}";
            } else {
                if ((content = streamReader(he.getRequestBody())) != null) {
                    LOGGER.info("*********JSON VALUE:{}", content);
                    resp = controller.updateTrendValues(content);
                } else {
                    resp = "{\"outcome\":\"RequestBody is invalid\"}";
                }
            }
            response = resp.getBytes();
            if (resp.contains(" successfully.")) {
                he.sendResponseHeaders(HttpURLConnection.HTTP_OK, response.length);
            } else if (resp.contains("Error: ")) {
                he.sendResponseHeaders(HttpURLConnection.HTTP_UNAVAILABLE, response.length);
            } else {
                he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, response.length);
            }
        } catch (IllegalArgumentException e) {
            LOGGER.warn(e.getMessage(), e);
            response = "{\"outcome\":\"IllegalArgumentException\"}".getBytes();
            he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, response.length);
        } catch (Exception e) {
            stackTrace = setStackTrace(e, (response = "{\"outcome\":\"Exception\"}".getBytes()), he);
        }

        respHeaders = he.getResponseHeaders();
        respHeaders.set("Content-Type", "application/json");
        os = he.getResponseBody();
        os.write(response);
        os.flush();
        he.close();

        if (stackTrace != null) {
            sendEvent(stackTrace);
        }
    }

    /**
     * HTTP DELETE handler
     *
     * @param he
     * @throws IOException
     */
    @Override
    public void doDelete(HttpExchange he) throws IOException {
        StackTraceElement[] stackTrace = null;
        byte[] response;
        OutputStream os;
        Headers respHeaders;
        String resp;
        String content;

        try {
            if (he.getRequestBody() == null) {
                resp = "{\"outcome\":\"RequestBody is invalid\"}";
            } else {
                if ((content = streamReader(he.getRequestBody())) != null) {
                    LOGGER.info("*********JSON VALUE:{}", content);
                    resp = controller.deleteTrendValues(content);
                } else {
                    resp = "{\"outcome\":\"RequestBody is invalid\"}";
                }
            }
            response = resp.getBytes();
            if (resp.contains(" successfully.")) {
                he.sendResponseHeaders(HttpURLConnection.HTTP_OK, response.length);
            } else if (resp.contains("Error: ")) {
                he.sendResponseHeaders(HttpURLConnection.HTTP_UNAVAILABLE, response.length);
            } else {
                he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, response.length);
            }
        } catch (IllegalArgumentException e) {
            LOGGER.warn(e.getMessage(), e);
            response = "{\"outcome\":\"IllegalArgumentException\"}".getBytes();
            he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, response.length);
        } catch (Exception e) {
            stackTrace = setStackTrace(e, (response = "{\"outcome\":\"Exception\"}".getBytes()), he);
        }

        respHeaders = he.getResponseHeaders();
        respHeaders.set("Content-Type", "application/json");
        os = he.getResponseBody();
        os.write(response);
        os.flush();
        he.close();

        if (stackTrace != null) {
            sendEvent(stackTrace);
        }
    }

    @Override
    public boolean getRunning() {
        return running;
    }

    /**
     * Returns the field 'LOGGER'
     *
     * @return Logger Object
     */
    @Override
    public Logger getLogger() {
        return LOGGER;
    }

    /**
     * Set the HttpExchange response header and return a StackTraceElement Array Object
     *
     * @param exception, Exception Object
     * @param response, byte Array Object
     * @param httpExchange, HttpExchange Object
     * @return StackTraceElement Array Object
     * @throws IOException
     */
    private StackTraceElement[] setStackTrace(final Exception exception, byte[] response, HttpExchange httpExchange) throws IOException {
        LOGGER.error(exception.getMessage(), exception);
        httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_UNAVAILABLE, response.length);
        return exception.getStackTrace();
    }
}
