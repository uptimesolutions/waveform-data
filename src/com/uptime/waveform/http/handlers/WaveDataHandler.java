/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.waveform.http.handlers;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.uptime.services.http.handler.AbstractGenericHandlerNew;
import static com.uptime.services.util.HttpUtils.parseQuery;
import static com.uptime.services.util.ServiceUtil.streamReader;
import static com.uptime.waveform.WaveformDataService.running;
import static com.uptime.waveform.WaveformDataService.sendEvent;
import com.uptime.waveform.controller.WaveDataController;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class WaveDataHandler extends AbstractGenericHandlerNew {
    private static final Logger LOGGER = LoggerFactory.getLogger(WaveDataHandler.class.getName());
    WaveDataController controller;

    public WaveDataHandler() {
        controller = new WaveDataController();
    }

    /**
     * HTTP GET handler
     *
     * @param he
     * @throws IOException
     */
    @Override
    public void doGet(HttpExchange he) throws IOException {
        StackTraceElement[] stackTrace = null;
        String resp;
        Map<String, Object> params;
        byte[] response;
        OutputStream os;
        Headers respHeaders;

        try {
            if ((params = parseQuery(he.getRequestURI().getRawQuery())).isEmpty()) {
                resp = "{\"outcome\":\"Hello World!\"}";
            } else if (params.containsKey("customerAccount") && params.containsKey("siteId") && params.containsKey("areaId") && params.containsKey("assetId") && params.containsKey("pointLocationId") && params.containsKey("pointId") && params.containsKey("sampleYear") && params.containsKey("sampleMonth") && params.containsKey("sampleTime")) {
                try {
                    LOGGER.info("customerAccount: {}, siteId: {}, areaId: {}, assetId: {}, pointLocationId: {}, pointId : {}, sampleYear: {}, sampleMonth: {}, sampleTime : {}", new Object[]{params.get("customerAccount").toString(), params.get("siteId").toString(), params.get("areaId").toString(), params.get("assetId").toString(), params.get("pointLocationId").toString(), params.get("pointId").toString(), params.get("sampleYear").toString(), params.get("sampleMonth").toString(), params.get("sampleTime").toString()});
                    resp = controller.getByCustomerSiteAreaAssetPointLocationPointsampleYearsampleMonthSampleTime(params.get("customerAccount").toString(), params.get("siteId").toString(), params.get("areaId").toString(), params.get("assetId").toString(), params.get("pointLocationId").toString(), params.get("pointId").toString(), params.get("sampleYear").toString(), params.get("sampleMonth").toString(), params.get("sampleTime").toString());
                } catch (NumberFormatException | NullPointerException e) {
                    resp = "{\"outcome\":\"A valid customerAccount, siteId, areaId, assetId, pointLocationId, pointId, sampleYear, sampleMonth and sampleTime are required.\"}";
                }
            } else {
                resp = "{\"outcome\":\"Unknown Params\"}";
            }

            response = resp.getBytes();
            if (resp.contains("Unknown Params") || (resp.contains("A valid") && resp.contains("required."))) {
                he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, response.length);
            } else if (resp.contains("Error: ")) {
                he.sendResponseHeaders(HttpURLConnection.HTTP_UNAVAILABLE, response.length);
            } else if (resp.contains("Null value received from database.")) {
                he.sendResponseHeaders(HttpURLConnection.HTTP_NOT_FOUND, response.length);
            } else {
                he.sendResponseHeaders(HttpURLConnection.HTTP_OK, response.length);
            }
        } catch (IllegalArgumentException e) {
            LOGGER.warn(e.getMessage(), e);
            response = "{\"outcome\":\"IllegalArgumentException\"}".getBytes();
            he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, response.length);
        } catch (ReadTimeoutException e) {
            stackTrace = setStackTrace(e, (response = "{\"outcome\":\"ReadTimeoutException\"}".getBytes()), he);
        } catch (UnavailableException e) {
            stackTrace = setStackTrace(e, (response = "{\"outcome\":\"UnavailableException\"}".getBytes()), he);
        } catch (UnsupportedEncodingException e) {
            stackTrace = setStackTrace(e, (response = "{\"outcome\":\"UnsupportedEncodingException\"}".getBytes()), he);
        } catch (Exception e) {
            stackTrace = setStackTrace(e, (response = "{\"outcome\":\"Exception\"}".getBytes()), he);
        }

        respHeaders = he.getResponseHeaders();
        respHeaders.set("Content-Type", "application/json");
        os = he.getResponseBody();
        os.write(response);
        os.flush();
        he.close();

        // Send Event
        if (stackTrace != null) {
            sendEvent(stackTrace);
        }
    }

    /**
     *
     * @param he
     * @throws IOException
     */
    @Override
    public void doPost(HttpExchange he) throws IOException {
        StackTraceElement[] stackTrace = null;
        byte[] response;
        OutputStream os;
        Headers respHeaders;
        String resp;
        String content;

        try {
            if (he.getRequestBody() == null) {
                resp = "{\"outcome\":\"RequestBody is invalid\"}";
            } else {
                if ((content = streamReader(he.getRequestBody())) != null) {
                    LOGGER.info("*********JSON VALUE:{}", content);
                    resp = controller.createWaveData(content);
                } else {
                    resp = "{\"outcome\":\"RequestBody is invalid\"}";
                }
            }
            response = resp.getBytes();
            if (resp.contains(" successfully.")) {
                he.sendResponseHeaders(HttpURLConnection.HTTP_OK, response.length);
            } else if (resp.contains("Error: ")) {
                he.sendResponseHeaders(HttpURLConnection.HTTP_UNAVAILABLE, response.length);
            } else {
                he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, response.length);
            }
        } catch (Exception e) {
            stackTrace = setStackTrace(e, (response = "{\"outcome\":\"Exception\"}".getBytes()), he);
        }

        respHeaders = he.getResponseHeaders();
        respHeaders.set("Content-Type", "application/json");
        os = he.getResponseBody();
        os.write(response);
        os.flush();
        he.close();

        if (stackTrace != null) {
            sendEvent(stackTrace);
        }
    }

    /**
     * HTTP PUT handler
     *
     * @param he
     * @throws IOException
     */
    @Override
    public void doPut(HttpExchange he) throws IOException {
        StackTraceElement[] stackTrace = null;
        byte[] response;
        OutputStream os;
        Headers respHeaders;
        String resp;
        String content;

        try {
            if (he.getRequestBody() == null) {
                resp = "{\"outcome\":\"RequestBody is invalid\"}";
            } else {
                if ((content = streamReader(he.getRequestBody())) != null) {
                    LOGGER.info("*********JSON VALUE:{}", content);
                    resp = controller.updateWaveData(content);
                } else {
                    resp = "{\"outcome\":\"RequestBody is invalid\"}";
                }
            }
            response = resp.getBytes();
            if (resp.contains(" successfully.")) {
                he.sendResponseHeaders(HttpURLConnection.HTTP_OK, response.length);
            } else if (resp.contains("Error: ")) {
                he.sendResponseHeaders(HttpURLConnection.HTTP_UNAVAILABLE, response.length);
            } else {
                he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, response.length);
            }
        } catch (IllegalArgumentException e) {
            LOGGER.warn(e.getMessage(), e);
            response = "{\"outcome\":\"IllegalArgumentException\"}".getBytes();
            he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, response.length);
        } catch (Exception e) {
            stackTrace = setStackTrace(e, (response = "{\"outcome\":\"Exception\"}".getBytes()), he);
        }

        respHeaders = he.getResponseHeaders();
        respHeaders.set("Content-Type", "application/json");
        os = he.getResponseBody();
        os.write(response);
        os.flush();
        he.close();

        if (stackTrace != null) {
            sendEvent(stackTrace);
        }
    }

    /**
     * HTTP DELETE handler
     *
     * @param he
     * @throws IOException
     */
    @Override
    public void doDelete(HttpExchange he) throws IOException {
        StackTraceElement[] stackTrace = null;
        byte[] response;
        OutputStream os;
        Headers respHeaders;
        String resp;
        String content;

        try {
            if (he.getRequestBody() == null) {
                resp = "{\"outcome\":\"RequestBody is invalid\"}";
            } else {
                if ((content = streamReader(he.getRequestBody())) != null) {
                    LOGGER.info("*********JSON VALUE:{}", content);
                    resp = controller.deleteWaveData(content);
                } else {
                    resp = "{\"outcome\":\"RequestBody is invalid\"}";
                }
            }
            response = resp.getBytes();
            if (resp.contains(" successfully.") || resp.contains("No WaveData items found")) {
                he.sendResponseHeaders(HttpURLConnection.HTTP_OK, response.length);
            } else if (resp.contains("Error: ")) {
                he.sendResponseHeaders(HttpURLConnection.HTTP_UNAVAILABLE, response.length);
            } else {
                he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, response.length);
            }
        } catch (IllegalArgumentException e) {
            LOGGER.warn(e.getMessage(), e);
            response = "{\"outcome\":\"IllegalArgumentException\"}".getBytes();
            he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, response.length);
        } catch (Exception e) {
            stackTrace = setStackTrace(e, (response = "{\"outcome\":\"Exception\"}".getBytes()), he);
        }

        respHeaders = he.getResponseHeaders();
        respHeaders.set("Content-Type", "application/json");
        os = he.getResponseBody();
        os.write(response);
        os.flush();
        he.close();

        if (stackTrace != null) {
            sendEvent(stackTrace);
        }
    }

    @Override
    public boolean getRunning() {
        return running;
    }

    /**
     * Returns the field 'LOGGER'
     *
     * @return Logger Object
     */
    @Override
    public Logger getLogger() {
        return LOGGER;
    }

    /**
     * Set the HttpExchange response header and return a StackTraceElement Array Object
     *
     * @param exception, Exception Object
     * @param response, byte Array Object
     * @param httpExchange, HttpExchange Object
     * @return StackTraceElement Array Object
     * @throws IOException
     */
    private StackTraceElement[] setStackTrace(final Exception exception, byte[] response, HttpExchange httpExchange) throws IOException {
        LOGGER.error(exception.getMessage(), exception);
        httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_UNAVAILABLE, response.length);
        return exception.getStackTrace();
    }
}
