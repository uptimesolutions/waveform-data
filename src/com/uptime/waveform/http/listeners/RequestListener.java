/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.waveform.http.listeners;

import com.sun.net.httpserver.HttpServer;
import com.uptime.waveform.http.handlers.SystemHandler;
import com.uptime.waveform.http.handlers.TrendHandler;
import com.uptime.waveform.http.handlers.WaveDataHandler;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Mohd Juned Alam
 */
public class RequestListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(RequestListener.class.getName());
    private int port = 0;
    HttpServer server;

    /**
     * Parameterized Constructor
     *
     * @param port, int
     */
    public RequestListener(int port) {
        this.port = port;
    }

    /**
     * Stop the Http server
     */
    public void stop() {
        LOGGER.info("Stopping request listener...");
        server.stop(10);
    }

    /**
     * Start the Http Server with the needed handlers
     *
     * @throws Exception
     */
    public void start() throws Exception {
        //***********************************TO DO*************************************
        // To add a new handler, create the class in the package "handler". The new class
        // needs to extend the abstract class AbstractGenericHandler. All abstract methods need to be
        // implemented. Once the class has been created, instantiate it in this method and add the
        // needed path to reach the handler.
        //
        // Note: The SystemHandler.java is the only handler that need to extend the abstract method
        // AbstractSystemHandler.
        //*****************************************************************************

        server = HttpServer.create(new InetSocketAddress(port), 2);
        server.createContext("/system", new SystemHandler());
        server.createContext("/trend", new TrendHandler());
        server.createContext("/wavedata", new WaveDataHandler());
        server.setExecutor(Executors.newCachedThreadPool());

        //start server
        LOGGER.info("Starting request listener...");
        server.start();
    }
}
