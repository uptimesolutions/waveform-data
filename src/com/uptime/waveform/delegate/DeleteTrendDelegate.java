/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.waveform.delegate;

import com.uptime.cassandra.trend.dao.TrendAcValuesDAO;
import com.uptime.cassandra.trend.dao.TrendDcValuesDAO;
import com.uptime.cassandra.trend.dao.TrendMapperImpl;
import com.uptime.cassandra.trend.entity.TrendAcValues;
import com.uptime.cassandra.trend.entity.TrendDcValues;
import static com.uptime.waveform.WaveformDataService.sendEvent;
import com.uptime.waveform.vo.WaveFormDataVO;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class DeleteTrendDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteTrendDelegate.class.getName()); 
    private final TrendAcValuesDAO trendAcValuesDAO;
    private final TrendDcValuesDAO trendDcValuesDAO;

    public DeleteTrendDelegate() {
        trendAcValuesDAO = TrendMapperImpl.getInstance().trendAcValuesDAO();
        trendDcValuesDAO = TrendMapperImpl.getInstance().trendDcValuesDAO();
    }
    
    /**
     * Delete Rows from Cassandra trend_ac_values table based on the given object
     *
     * @param waveFormDataVO, WaveFormDataVO object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String deleteTrendAcValues(WaveFormDataVO waveFormDataVO) throws IllegalArgumentException {
        List<TrendAcValues> trendValuesList;
        TrendAcValues trendValues = null;

        // Find the entities based on the given values
        try {
            if ((trendValuesList = trendAcValuesDAO.findByPK(waveFormDataVO.getCustomerAccount(), waveFormDataVO.getSiteId(), waveFormDataVO.getAreaId(), waveFormDataVO.getAssetId(), waveFormDataVO.getPointLocationId(), waveFormDataVO.getPointId(), waveFormDataVO.getApSetId(), waveFormDataVO.getParamName(), waveFormDataVO.getSampleTime())) != null && !trendValuesList.isEmpty()) {
                trendValues = trendValuesList.get(0);
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find items to Delete.\"}";
        }

        // Delete the entities from Cassandra
        try {
            trendAcValuesDAO.delete(trendValues);
            return "{\"outcome\":\"Deleted AC Trend Values successfully.\"}";
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to Delete AC Trend Values.\"}";
    }
    
    /**
     * Delete Rows from Cassandra trend_dc_values table based on the given object
     *
     * @param waveFormDataVO, WaveFormDataVO object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String deleteTrendDcValues(WaveFormDataVO waveFormDataVO) throws IllegalArgumentException {
        List<TrendDcValues> trendValuesList;
        TrendDcValues trendValues = null;

        // Find the entities based on the given values
        try {
            if ((trendValuesList = trendDcValuesDAO.findByPK(waveFormDataVO.getCustomerAccount(), waveFormDataVO.getSiteId(), waveFormDataVO.getAreaId(), waveFormDataVO.getAssetId(), waveFormDataVO.getPointLocationId(), waveFormDataVO.getPointId(), waveFormDataVO.getApSetId(), waveFormDataVO.getSampleTime())) != null && !trendValuesList.isEmpty()) {
                trendValues = trendValuesList.get(0);
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find items to Delete.\"}";
        }

        // Delete the entities from Cassandra
        try {
            trendDcValuesDAO.delete(trendValues);
            return "{\"outcome\":\"Deleted DC Trend Values successfully.\"}";
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to Delete DC Trend Values.\"}";
    }
    
}
