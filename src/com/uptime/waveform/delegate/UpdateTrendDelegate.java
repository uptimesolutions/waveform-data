/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.waveform.delegate;

import com.uptime.cassandra.trend.dao.TrendAcValuesDAO;
import com.uptime.cassandra.trend.dao.TrendDcValuesDAO;
import com.uptime.cassandra.trend.dao.TrendMapperImpl;
import com.uptime.cassandra.trend.entity.TrendAcValues;
import com.uptime.cassandra.trend.entity.TrendDcValues;
import com.uptime.services.util.ServiceUtil;
import static com.uptime.waveform.WaveformDataService.sendEvent;
import com.uptime.waveform.utils.DelegateUtil;
import com.uptime.waveform.vo.WaveFormDataVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class UpdateTrendDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateTrendDelegate.class.getName()); 
    private final TrendAcValuesDAO trendAcValuesDAO;
    private final TrendDcValuesDAO trendDcValuesDAO;

    public UpdateTrendDelegate() {
        trendAcValuesDAO = TrendMapperImpl.getInstance().trendAcValuesDAO();
        trendDcValuesDAO = TrendMapperImpl.getInstance().trendDcValuesDAO();
    }

    /**
     * Update Rows in Cassandra trend_ac_values table based on the given object
     *
     * @param waveFormDataVO, WaveFormDataVO object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String updateTrendAcValues(WaveFormDataVO waveFormDataVO) throws IllegalArgumentException {
        TrendAcValues newAcTrendValues;
        String errorMsg;
        
        // Insert updated entities into Cassandra
        if (waveFormDataVO != null) {
            newAcTrendValues = DelegateUtil.getTrendAcValues(waveFormDataVO);

            try {
                if ((errorMsg = ServiceUtil.validateObjectData(newAcTrendValues)) == null) {
                    trendAcValuesDAO.create(newAcTrendValues);
                    return "{\"outcome\":\"Updated AC Trend Values successfully.\"}";
                } else {
                    return errorMsg;
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }
            
        }

        return "{\"outcome\":\"Error: Failed to update AC Trend Values.\"}";
    }
    
    /**
     * Update Rows in Cassandra trend_dc_values table based on the given object
     *
     * @param waveFormDataVO, WaveFormDataVO object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String updateTrendDcValues(WaveFormDataVO waveFormDataVO) throws IllegalArgumentException {
        TrendDcValues newDcTrendValues;
        String errorMsg;
        
        // Insert updated entities into Cassandra
        if (waveFormDataVO != null) {
            newDcTrendValues = DelegateUtil.getTrendDcValues(waveFormDataVO);

            try {
                if ((errorMsg = ServiceUtil.validateObjectData(newDcTrendValues)) == null) {
                    trendDcValuesDAO.create(newDcTrendValues);
                    return "{\"outcome\":\"Updated DC Trend Values successfully.\"}";
                } else {
                    return errorMsg;
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }
            
        }

        return "{\"outcome\":\"Error: Failed to update DC Trend Values.\"}";
    }
}
