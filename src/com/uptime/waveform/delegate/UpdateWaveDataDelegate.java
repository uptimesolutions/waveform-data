/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.waveform.delegate;

import com.uptime.cassandra.trend.dao.TrendMapperImpl;
import com.uptime.cassandra.trend.dao.WaveDataDAO;
import com.uptime.cassandra.trend.entity.WaveData;
import com.uptime.services.util.ServiceUtil;
import static com.uptime.waveform.WaveformDataService.sendEvent;
import com.uptime.waveform.utils.DelegateUtil;
import com.uptime.waveform.vo.WaveFormDataVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class UpdateWaveDataDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateWaveDataDelegate.class.getName()); 
    private final WaveDataDAO waveDataDAO;

    public UpdateWaveDataDelegate() {
        waveDataDAO = TrendMapperImpl.getInstance().waveDataDAO();
    }

    /**
     * Update Rows in Cassandra wave_data table based on the given object
     *
     * @param waveFormDataVO, WaveFormDataVO object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String updateWaveData(WaveFormDataVO waveFormDataVO) throws IllegalArgumentException {
        WaveData newWaveData;
        String errorMsg;
        
        // Insert updated entities into Cassandra
        if (waveFormDataVO != null) {
            newWaveData = DelegateUtil.getWaveData(waveFormDataVO);

            try {
                if ((errorMsg = ServiceUtil.validateObjectData(newWaveData)) == null) {
                    waveDataDAO.create(newWaveData);
                    return "{\"outcome\":\"Updated WaveData Values successfully.\"}";
                } else {
                    return errorMsg;
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }
        }

        return "{\"outcome\":\"Error: Failed to update WaveData Values.\"}";
    }
   
}
