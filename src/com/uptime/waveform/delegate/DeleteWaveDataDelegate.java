/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.waveform.delegate;

import com.uptime.cassandra.trend.dao.TrendMapperImpl;
import com.uptime.cassandra.trend.dao.WaveDataDAO;
import com.uptime.cassandra.trend.entity.WaveData;
import static com.uptime.waveform.WaveformDataService.sendEvent;
import com.uptime.waveform.vo.WaveFormDataVO;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class DeleteWaveDataDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteWaveDataDelegate.class.getName()); 
    private final WaveDataDAO waveDataDAO;

    public DeleteWaveDataDelegate() {
        waveDataDAO = TrendMapperImpl.getInstance().waveDataDAO();
    }
    
    /**
     * Delete Rows from Cassandra wave-data table based on the given object
     *
     * @param waveFormDataVO, WaveFormDataVO object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String deleteWaveData(WaveFormDataVO waveFormDataVO) throws IllegalArgumentException {
        List<WaveData> waveDataList;
         WaveData originalWaveData = null;

        // Find the entities based on the given values
        try {
            if ((waveDataList = waveDataDAO.findByPK(waveFormDataVO.getCustomerAccount(), waveFormDataVO.getSiteId(), waveFormDataVO.getAreaId(), waveFormDataVO.getAssetId(), waveFormDataVO.getPointLocationId(), waveFormDataVO.getPointId(), waveFormDataVO.getSampleYear(), waveFormDataVO.getSampleMonth(), waveFormDataVO.getSampleTime())) != null && !waveDataList.isEmpty()){
                originalWaveData = waveDataList.get(0);
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find items to Delete.\"}";
        }

        // Delete the entities from Cassandra
        try {
            waveDataDAO.delete(originalWaveData);
            return "{\"outcome\":\"Deleted WaveData Values successfully.\"}";
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to Delete WaveData Values.\"}";
    }
}
