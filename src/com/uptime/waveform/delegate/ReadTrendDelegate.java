/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.waveform.delegate;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.trend.dao.TrendAcValuesDAO;
import com.uptime.cassandra.trend.dao.TrendDcValuesDAO;
import com.uptime.cassandra.trend.dao.TrendMapperImpl;
import com.uptime.cassandra.trend.entity.TrendAcValues;
import com.uptime.cassandra.trend.entity.TrendDcValues;
import java.time.ZoneOffset;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
public class ReadTrendDelegate {

    private final TrendAcValuesDAO trendAcValuesDAO;
    private final TrendDcValuesDAO trendDcValuesDAO;

    public ReadTrendDelegate() {
        trendAcValuesDAO = TrendMapperImpl.getInstance().trendAcValuesDAO();
        trendDcValuesDAO = TrendMapperImpl.getInstance().trendDcValuesDAO();
    }
    
    /**
     *
     * Returns a List of TrendAcValues Objects for the given customerAcct, siteId, areaId, assetId, 
     * pointLocationId, pointId, apSetId, paramName and sampleTime from trend_ac_values table
     * 
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @param pointLocationId, String Object
     * @param pointId, String Object
     * @param apSetId, String Object
     * @param paramName, String Object
     * @param sampleTime, String Object
     * 
     * @return List of TrendAcValues Objects
     * @throws UnavailableException
     * @throws ReadTimeoutException
     * @throws IllegalArgumentException
     */
    public List<TrendAcValues> getByCustomerSiteAreaAssetPointLocationPointApSetParamSampleTime(String customer, String siteId, String areaId, String assetId, String pointLocationId, String pointId, String apSetId, String paramName, String sampleTime) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return trendAcValuesDAO.findByPK(customer, UUID.fromString(siteId), UUID.fromString(areaId), UUID.fromString(assetId), UUID.fromString(pointLocationId), UUID.fromString(pointId), UUID.fromString(apSetId), paramName, Instant.parse(sampleTime).atZone(ZoneOffset.UTC).toInstant());
        
    }

    /**
     *
     * Returns a List of TrendAcValues Objects for the given customerAcct, siteId, areaId, assetId, 
     * pointLocationId, pointId, apSetId, paramName and sampleTime from trend_ac_values table
     * 
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @param pointLocationId, String Object
     * @param pointId, String Object
     * @param apSetId, String Object
     * @param paramName, String Object
     * @param startTime , String Object
     * @param endTime , String Object
     * 
     * @return List of TrendAcValues Objects
     * @throws UnavailableException
     * @throws ReadTimeoutException
     * @throws IllegalArgumentException
     */
    public List<TrendAcValues> queryByDateRange(String customer, String siteId, String areaId, String assetId, String pointLocationId, String pointId, String apSetId, String paramName, String startTime, String endTime) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return trendAcValuesDAO.findByDateRange(customer, UUID.fromString(siteId), UUID.fromString(areaId), UUID.fromString(assetId), UUID.fromString(pointLocationId), UUID.fromString(pointId), UUID.fromString(apSetId), paramName,  Instant.parse(startTime).atZone(ZoneOffset.UTC).toInstant(), Instant.parse(endTime).atZone(ZoneOffset.UTC).toInstant());
    }

    /**
     *
     * Returns a List of TrendAcValues Objects for the given customerAcct, siteId, areaId, assetId, 
     * pointLocationId, pointId, apSetId, paramName and sampleTime from trend_ac_values table
     * 
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @param pointLocationId, String Object
     * @param pointId, String Object
     * @param apSetId, String Object
     * @param startTime , String Object
     * @param endTime , String Object
     * 
     * @return List of TrendAcValues Objects
     * @throws UnavailableException
     * @throws ReadTimeoutException
     * @throws IllegalArgumentException
     */
    public List<TrendDcValues> queryByDateRange(String customer, String siteId, String areaId, String assetId, String pointLocationId, String pointId, String apSetId, String startTime, String endTime) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return trendDcValuesDAO.findByDateRange(customer, UUID.fromString(siteId), UUID.fromString(areaId), UUID.fromString(assetId), UUID.fromString(pointLocationId), UUID.fromString(pointId), UUID.fromString(apSetId), Instant.parse(startTime).atZone(ZoneOffset.UTC).toInstant(), Instant.parse(endTime).atZone(ZoneOffset.UTC).toInstant());
    }

    /**
     *
     * Returns a List of TrendAcValues Objects for the given customerAcct, siteId, areaId,
     * assetId, pointLocationId, pointId, apSetId, paramName and limit from trend_ac_values table
     * 
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @param pointLocationId, String Object
     * @param pointId, String Object
     * @param apSetId, String Object
     * @param paramName, String Object
     * @param limit, String Object
     * 
     * @return List of TrendAcValues Objects
     * @throws UnavailableException
     * @throws ReadTimeoutException
     * @throws IllegalArgumentException
     */
    public List<TrendAcValues> getByCustomerSiteAreaAssetPointLocationPointApSetParamLimit(String customer, String siteId, String areaId, String assetId, String pointLocationId, String pointId, String apSetId, String paramName, String limit) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return trendAcValuesDAO.findTrend(customer, UUID.fromString(siteId), UUID.fromString(areaId), UUID.fromString(assetId), UUID.fromString(pointLocationId), UUID.fromString(pointId), UUID.fromString(apSetId), paramName, Integer.parseInt(limit));
    }
    
    /**
     *
     * Returns a List of TrendDcValues Objects for the given customerAcct, siteId, areaId,
     * assetId, pointLocationId, pointId, apSetId and sampleTime from trend_dc_values table
     * 
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @param pointLocationId, String Object
     * @param pointId, String Object
     * @param apSetId, String Object
     * @param sampleTime, String Object
     * 
     * @return List of TrendAcValues Objects
     * @throws UnavailableException
     * @throws ReadTimeoutException
     * @throws IllegalArgumentException
     */
    public List<TrendDcValues> getByCustomerSiteAreaAssetPointLocationPointApSetSampleTime(String customer, String siteId, String areaId, String assetId, String pointLocationId, String pointId, String apSetId, String sampleTime) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return trendDcValuesDAO.findByPK(customer, UUID.fromString(siteId), UUID.fromString(areaId), UUID.fromString(assetId), UUID.fromString(pointLocationId), UUID.fromString(pointId), UUID.fromString(apSetId), Instant.parse(sampleTime).atZone(ZoneOffset.UTC).toInstant());
    }
    
    /**
     *
     * Returns a List of TrendDcValues Objects for the given customerAcct, siteId, areaId,
     * assetId, pointLocationId, pointId, apSetId and limit from trend_dc_values table
     * 
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @param pointLocationId, String Object
     * @param pointId, String Object
     * @param apSetId, String Object
     * @param limit, String Object
     * 
     * @return List of TrendAcValues Objects
     * @throws UnavailableException
     * @throws ReadTimeoutException
     * @throws IllegalArgumentException
     */
    public List<TrendDcValues> getByCustomerSiteAreaAssetPointLocationPointApSetLimit(String customer, String siteId, String areaId, String assetId, String pointLocationId, String pointId, String apSetId, String limit) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return trendDcValuesDAO.findTrend(customer, UUID.fromString(siteId), UUID.fromString(areaId), UUID.fromString(assetId), UUID.fromString(pointLocationId), UUID.fromString(pointId), UUID.fromString(apSetId), Integer.parseInt(limit));
    }

}
