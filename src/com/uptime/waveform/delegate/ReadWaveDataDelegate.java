/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.waveform.delegate;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.trend.dao.TrendMapperImpl;
import com.uptime.cassandra.trend.dao.WaveDataDAO;
import com.uptime.cassandra.trend.entity.WaveData;
import java.time.ZoneOffset;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
public class ReadWaveDataDelegate {

    private final WaveDataDAO waveDataDAO;

    public ReadWaveDataDelegate() {
        waveDataDAO = TrendMapperImpl.getInstance().waveDataDAO();
    }
    
    /**
     *
     * Returns a List of WaveData Objects for the given customerAcct, siteId, areaId,
     * assetId, pointLocationId, pointId, sampleYear, sampleMonth and sampleTime from wave_data table
     * 
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @param pointLocationId, String Object
     * @param pointId, String Object
     * @param sampleYear, String Object
     * @param sampleMonth, String Object
     * @param sampleTime, String Object
     * 
     * @return List of WaveData Objects
     * @throws UnavailableException
     * @throws ReadTimeoutException
     * @throws IllegalArgumentException
     */
    public List<WaveData> getByCustomerSiteAreaAssetPointLocationPointSampleYearSampleMonthSampleTime(String customer, String siteId, String areaId, String assetId, String pointLocationId, String pointId, String sampleYear, String sampleMonth, String sampleTime) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return waveDataDAO.findByPK(customer, UUID.fromString(siteId), UUID.fromString(areaId), UUID.fromString(assetId), UUID.fromString(pointLocationId), UUID.fromString(pointId), Short.parseShort(sampleYear), Byte.parseByte(sampleMonth), Instant.parse(sampleTime).atZone(ZoneOffset.UTC).toInstant());
        
    }
}
