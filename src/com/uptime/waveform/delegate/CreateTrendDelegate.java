/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.waveform.delegate;

import com.uptime.cassandra.trend.dao.TrendAcValuesDAO;
import com.uptime.cassandra.trend.dao.TrendDcValuesDAO;
import com.uptime.cassandra.trend.dao.TrendMapperImpl;
import com.uptime.cassandra.trend.entity.TrendAcValues;
import com.uptime.cassandra.trend.entity.TrendDcValues;
import com.uptime.services.util.ServiceUtil;
import static com.uptime.waveform.WaveformDataService.sendEvent;
import com.uptime.waveform.utils.DelegateUtil;
import com.uptime.waveform.vo.WaveFormDataVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class CreateTrendDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateTrendDelegate.class.getName()); 
    private final TrendAcValuesDAO trendAcValuesDAO;
    private final TrendDcValuesDAO trendDcValuesDAO;

    public CreateTrendDelegate() {
        trendAcValuesDAO = TrendMapperImpl.getInstance().trendAcValuesDAO();
        trendDcValuesDAO = TrendMapperImpl.getInstance().trendDcValuesDAO();
    }

    /**
     * Insert new Row into Cassandra trend_ac_values table based on the given object
     *
     * @param waveFormDataVO, WaveFormDataVO object
     * @return String object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String createTrendAcValues(WaveFormDataVO waveFormDataVO) throws IllegalArgumentException, Exception {
        TrendAcValues trendAcValues = DelegateUtil.getTrendAcValues(waveFormDataVO);
        String errorMsg;
        
        // Insert the entities into Cassandra
        try {
            if ((errorMsg = ServiceUtil.validateObjectData(trendAcValues)) == null) {
                trendAcValuesDAO.create(trendAcValues);
                return "{\"outcome\":\"New AC Trend Values created successfully.\"}";
            } else {
                return errorMsg;
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to create AC Trend Values.\"}";
    }

    /**
     * Insert new Row into Cassandra trend_dc_values table based on the given object
     *
     * @param waveFormDataVO, WaveFormDataVO object
     * @return String object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String createTrendDcValues(WaveFormDataVO waveFormDataVO) throws IllegalArgumentException, Exception {
        TrendDcValues trendDcValues = DelegateUtil.getTrendDcValues(waveFormDataVO);
        String errorMsg;
        
        // Insert the entities into Cassandra
        try {
            
            if ((errorMsg = ServiceUtil.validateObjectData(trendDcValues)) == null) {
                trendDcValuesDAO.create(trendDcValues);
                return "{\"outcome\":\"New DC Trend Values created successfully.\"}";
            } else {
                return errorMsg;
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to create DC Trend Values.\"}";
    }

}
