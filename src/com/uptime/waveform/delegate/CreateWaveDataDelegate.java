/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.waveform.delegate;

import com.uptime.cassandra.trend.dao.TrendMapperImpl;
import com.uptime.cassandra.trend.dao.WaveDataDAO;
import com.uptime.cassandra.trend.entity.WaveData;
import com.uptime.services.util.ServiceUtil;
import static com.uptime.waveform.WaveformDataService.sendEvent;
import com.uptime.waveform.utils.DelegateUtil;
import com.uptime.waveform.vo.WaveFormDataVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class CreateWaveDataDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateWaveDataDelegate.class.getName()); 
    private final WaveDataDAO waveDataDAO;

    public CreateWaveDataDelegate() {
        waveDataDAO = TrendMapperImpl.getInstance().waveDataDAO();
    }

    /**
     * Insert new Row into Cassandra wave_data table based on the given object
     *
     * @param waveFormDataVO, WaveFormDataVO object
     * @return String object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String createWaveData(WaveFormDataVO waveFormDataVO) throws IllegalArgumentException, Exception {
        WaveData waveData;
        String errorMsg;
        
        waveData = DelegateUtil.getWaveData(waveFormDataVO);

        // Insert the entities into Cassandra
        try {
            if ((errorMsg = ServiceUtil.validateObjectData(waveData)) == null) {
                waveDataDAO.create(waveData);
                return "{\"outcome\":\"New WaveData created successfully.\"}";
            } else {
                return errorMsg;
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to create WaveData.\"}";
    }
}
