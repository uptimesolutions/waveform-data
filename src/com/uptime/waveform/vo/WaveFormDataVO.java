/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.waveform.vo;

import com.uptime.cassandra.trend.entity.Waveform;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author Mohd Juned Alam
 */
public class WaveFormDataVO {

    private String customerAccount;
    private UUID siteId;
    private UUID areaId;
    private UUID assetId;
    private UUID pointLocationId;
    private UUID pointId;
    private UUID apSetId;
    private String paramName;
    private UUID alSetId;
    private Instant sampleTime;
    private String label;
    private String units;
    private float value;
    
    private String notes;
    private float highAlert;
    private float highFault;
    private float lowAlert;
    private float lowFault;
    private String sensorType;
    
    private short sampleYear;
    private byte sampleMonth;
    private float sensitivityValue;
    private Waveform waveform;
    
    public WaveFormDataVO() {

    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getAreaId() {
        return areaId;
    }

    public void setAreaId(UUID areaId) {
        this.areaId = areaId;
    }

    public UUID getAssetId() {
        return assetId;
    }

    public void setAssetId(UUID assetId) {
        this.assetId = assetId;
    }

    public UUID getPointLocationId() {
        return pointLocationId;
    }

    public void setPointLocationId(UUID pointLocationId) {
        this.pointLocationId = pointLocationId;
    }

    public UUID getPointId() {
        return pointId;
    }

    public void setPointId(UUID pointId) {
        this.pointId = pointId;
    }

    public UUID getApSetId() {
        return apSetId;
    }

    public void setApSetId(UUID apSetId) {
        this.apSetId = apSetId;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public Instant getSampleTime() {
        return sampleTime;
    }

    public void setSampleTime(Instant sampleTime) {
        this.sampleTime = sampleTime;
    }

    public UUID getAlSetId() {
        return alSetId;
    }

    public void setAlSetId(UUID alSetId) {
        this.alSetId = alSetId;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }


    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public float getHighAlert() {
        return highAlert;
    }

    public void setHighAlert(float highAlert) {
        this.highAlert = highAlert;
    }

    public float getHighFault() {
        return highFault;
    }

    public void setHighFault(float highFault) {
        this.highFault = highFault;
    }

    public float getLowAlert() {
        return lowAlert;
    }

    public void setLowAlert(float lowAlert) {
        this.lowAlert = lowAlert;
    }

    public float getLowFault() {
        return lowFault;
    }

    public void setLowFault(float lowFault) {
        this.lowFault = lowFault;
    }

    public String getSensorType() {
        return sensorType;
    }

    public void setSensorType(String sensorType) {
        this.sensorType = sensorType;
    }

    public short getSampleYear() {
        return sampleYear;
    }

    public void setSampleYear(short sampleYear) {
        this.sampleYear = sampleYear;
    }

    public byte getSampleMonth() {
        return sampleMonth;
    }

    public void setSampleMonth(byte sampleMonth) {
        this.sampleMonth = sampleMonth;
    }

    public float getSensitivityValue() {
        return sensitivityValue;
    }

    public void setSensitivityValue(float sensitivityValue) {
        this.sensitivityValue = sensitivityValue;
    }

    public Waveform getWaveform() {
        return waveform;
    }

    public void setWaveform(Waveform waveform) {
        this.waveform = waveform;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.customerAccount);
        hash = 79 * hash + Objects.hashCode(this.siteId);
        hash = 79 * hash + Objects.hashCode(this.areaId);
        hash = 79 * hash + Objects.hashCode(this.assetId);
        hash = 79 * hash + Objects.hashCode(this.pointLocationId);
        hash = 79 * hash + Objects.hashCode(this.pointId);
        hash = 79 * hash + Objects.hashCode(this.apSetId);
        hash = 79 * hash + Objects.hashCode(this.paramName);
        hash = 79 * hash + Objects.hashCode(this.alSetId);
        hash = 79 * hash + Objects.hashCode(this.sampleTime);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WaveFormDataVO other = (WaveFormDataVO) obj;
        if (Float.floatToIntBits(this.value) != Float.floatToIntBits(other.value)) {
            return false;
        }
        if (Float.floatToIntBits(this.highAlert) != Float.floatToIntBits(other.highAlert)) {
            return false;
        }
        if (Float.floatToIntBits(this.highFault) != Float.floatToIntBits(other.highFault)) {
            return false;
        }
        if (Float.floatToIntBits(this.lowAlert) != Float.floatToIntBits(other.lowAlert)) {
            return false;
        }
        if (Float.floatToIntBits(this.lowFault) != Float.floatToIntBits(other.lowFault)) {
            return false;
        }
        if (this.sampleYear != other.sampleYear) {
            return false;
        }
        if (this.sampleMonth != other.sampleMonth) {
            return false;
        }
        if (Float.floatToIntBits(this.sensitivityValue) != Float.floatToIntBits(other.sensitivityValue)) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.label, other.label)) {
            return false;
        }
        if (!Objects.equals(this.units, other.units)) {
            return false;
        }
        if (!Objects.equals(this.notes, other.notes)) {
            return false;
        }
        if (!Objects.equals(this.sensorType, other.sensorType)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.areaId, other.areaId)) {
            return false;
        }
        if (!Objects.equals(this.assetId, other.assetId)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationId, other.pointLocationId)) {
            return false;
        }
        if (!Objects.equals(this.pointId, other.pointId)) {
            return false;
        }
        if (!Objects.equals(this.apSetId, other.apSetId)) {
            return false;
        }
        if (!Objects.equals(this.paramName, other.paramName)) {
            return false;
        }
        if (!Objects.equals(this.alSetId, other.alSetId)) {
            return false;
        }
        if (!Objects.equals(this.sampleTime, other.sampleTime)) {
            return false;
        }
        if (!Objects.equals(this.waveform, other.waveform)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "WaveFormDataVO{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", areaId=" + areaId + ", assetId=" + assetId + ", pointLocationId=" + pointLocationId + ", pointId=" + pointId + ", apSetId=" + apSetId + ", paramName=" + paramName + ", alSetId=" + alSetId + ", sampleTime=" + sampleTime + ", label=" + label + ", units=" + units + ", value=" + value + ", notes=" + notes + ", highAlert=" + highAlert + ", highFault=" + highFault + ", lowAlert=" + lowAlert + ", lowFault=" + lowFault + ", sensorType=" + sensorType + ", sampleYear=" + sampleYear + ", sampleMonth=" + sampleMonth + ", sensitivityValue=" + sensitivityValue + ", waveform=" + waveform + '}';
    }
}
