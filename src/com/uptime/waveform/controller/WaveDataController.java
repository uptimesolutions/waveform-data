/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.waveform.controller;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.trend.entity.WaveData;
import com.uptime.services.util.JsonConverterUtil;
import com.uptime.waveform.delegate.CreateWaveDataDelegate;
import com.uptime.waveform.delegate.DeleteWaveDataDelegate;
import com.uptime.waveform.delegate.ReadWaveDataDelegate;
import com.uptime.waveform.delegate.UpdateWaveDataDelegate;
import com.uptime.waveform.utils.JsonUtil;
import com.uptime.waveform.vo.WaveFormDataVO;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class WaveDataController {
    private static final Logger LOGGER = LoggerFactory.getLogger(WaveDataController.class.getName()); 
    private final ReadWaveDataDelegate readDelegate;

    public WaveDataController() {
        readDelegate =  new ReadWaveDataDelegate();
    }

    /**
     * Retrieve a List of WaveData Objects for the given customerAcct, siteId, areaId, assetId, 
     * pointLocationId, pointId, sampleYear, sampleMonth and sampleTime from wave_data table
     * 
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @param pointLocationId, String Object
     * @param pointId, String Object
     * @param sampleYear, String Object
     * @param sampleMonth, String Object
     * @param sampleTime, String Object
     * @return String Object
     * @throws UnavailableException
     * @throws ReadTimeoutException
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String getByCustomerSiteAreaAssetPointLocationPointsampleYearsampleMonthSampleTime(String customer, String siteId, String areaId, String assetId, String pointLocationId, String pointId, String sampleYear, String sampleMonth, String sampleTime) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<WaveData> result;
        StringBuilder json;

        if ((result = readDelegate.getByCustomerSiteAreaAssetPointLocationPointSampleYearSampleMonthSampleTime(customer, siteId, areaId, assetId, pointLocationId, pointId, sampleYear, sampleMonth, sampleTime)) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                    .append("{\"waveData\":").append(JsonConverterUtil.toJSON(result.get(0)))
                    .append(",\"outcome\":\"GET worked successfully.\"}");
            return json.toString();
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }
    
    /**
     * Create a new WaveFormData by inserting into wave_data table
     *
     * @param content, String Object
     * @return String Object
     */
    public String createWaveData(String content) {
        CreateWaveDataDelegate createDelegate;
        WaveFormDataVO waveFormDataVO;
        LocalDateTime ld;
        
        if ((waveFormDataVO = JsonUtil.parser(content)) != null) {
            if (waveFormDataVO.getCustomerAccount() != null && !waveFormDataVO.getCustomerAccount().isEmpty()
                    && waveFormDataVO.getSiteId()!= null 
                    && waveFormDataVO.getAreaId()!= null
                    && waveFormDataVO.getAssetId() != null
                    && waveFormDataVO.getPointLocationId()!= null 
                    && waveFormDataVO.getPointId() != null 
                    && waveFormDataVO.getSampleTime() != null){

                // Insert into Cassandra
                try {
                    if (waveFormDataVO.getSampleYear() == 0 ||
                            waveFormDataVO.getSampleMonth() == 0) {
                        ld = LocalDateTime.ofInstant(waveFormDataVO.getSampleTime(), ZoneOffset.UTC);
                        waveFormDataVO.setSampleYear((short) ld.getYear());
                        waveFormDataVO.setSampleMonth((byte) ld.getMonth().getValue());
                    }
                    createDelegate = new CreateWaveDataDelegate();
                    return createDelegate.createWaveData(waveFormDataVO);
                } catch (IllegalArgumentException e) {
                    return "{\"outcome\":\"Error: Failed to create new WaveData do to IllegalArgumentException.\"}";
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                    return "{\"outcome\":\"Error: Failed to create new WaveData.\"}";
                }

            } else {
                return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Update wave_data table dealing with the given WaveData
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String updateWaveData(String content) throws IllegalArgumentException {
        WaveFormDataVO waveFormDataVO;
        UpdateWaveDataDelegate updateDelegate;
        
        if ((waveFormDataVO = JsonUtil.parser(content)) != null) {
            if (waveFormDataVO.getCustomerAccount() != null && !waveFormDataVO.getCustomerAccount().isEmpty()
                    && waveFormDataVO.getSiteId()!= null 
                    && waveFormDataVO.getAreaId()!= null
                    && waveFormDataVO.getAssetId() != null
                    && waveFormDataVO.getPointLocationId()!= null 
                    && waveFormDataVO.getPointId() != null 
                    && waveFormDataVO.getSampleYear() > 0
                    && waveFormDataVO.getSampleMonth() > 0
                    && waveFormDataVO.getSampleTime() != null){
                // Update in Cassandra
                try {
                    updateDelegate = new UpdateWaveDataDelegate();
                    return updateDelegate.updateWaveData(waveFormDataVO);
                } catch (IllegalArgumentException e) {
                    throw e;
                }

            } else {
                return "{\"outcome\":\"insufficient data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Delete from wave_data table dealing with the given WaveFormData
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String deleteWaveData(String content) throws IllegalArgumentException {
        DeleteWaveDataDelegate deleteDelegate;
        WaveFormDataVO waveFormDataVO;

        if ((waveFormDataVO = JsonUtil.parser(content)) != null) {
            if (waveFormDataVO.getCustomerAccount() != null && !waveFormDataVO.getCustomerAccount().isEmpty()
                    && waveFormDataVO.getSiteId() != null 
                    && waveFormDataVO.getAreaId() != null
                    && waveFormDataVO.getAssetId() != null
                    && waveFormDataVO.getPointLocationId() != null 
                    && waveFormDataVO.getPointId() != null 
                    && waveFormDataVO.getSampleYear() > 0
                    && waveFormDataVO.getSampleMonth() > 0
                    && waveFormDataVO.getSampleTime() != null){

                // Delete in Cassandra
                try {
                    deleteDelegate = new DeleteWaveDataDelegate();
                    return deleteDelegate.deleteWaveData(waveFormDataVO);
                } catch (IllegalArgumentException e) {
                    throw e;
                }

            } else {
                return "{\"outcome\":\"insufficient data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

}
