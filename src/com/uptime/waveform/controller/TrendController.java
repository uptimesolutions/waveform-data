/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.waveform.controller;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.trend.entity.TrendAcValues;
import com.uptime.cassandra.trend.entity.TrendDcValues;
import com.uptime.services.util.JsonConverterUtil;
import com.uptime.waveform.delegate.CreateTrendDelegate;
import com.uptime.waveform.delegate.DeleteTrendDelegate;
import com.uptime.waveform.delegate.ReadTrendDelegate;
import com.uptime.waveform.delegate.UpdateTrendDelegate;
import com.uptime.waveform.utils.JsonUtil;
import com.uptime.waveform.vo.WaveFormDataVO;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class TrendController {
    private static final Logger LOGGER = LoggerFactory.getLogger(TrendController.class.getName()); 
    private final ReadTrendDelegate readDelegate;

    public TrendController() {
        readDelegate = new ReadTrendDelegate();
    }

    /**
     * Retrieve a List of TrendAcValues Objects for the given customerAcct, siteId, areaId, assetId, pointLocationId, pointId, apSetId, paramName and sampleTime from trend_ac_values table
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @param pointLocationId, String Object
     * @param pointId, String Object
     * @param apSetId, String Object
     * @param paramName, String Object
     * @param sampleTime, String Object
     * @return String Object
     * @throws UnavailableException
     * @throws ReadTimeoutException
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String getByCustomerSiteAreaAssetPointLocationPointApSetParamSampleTime(String customer, String siteId, String areaId, String assetId, String pointLocationId, String pointId, String apSetId, String paramName, String sampleTime) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<TrendAcValues> result;
        StringBuilder json;

        if ((result = readDelegate.getByCustomerSiteAreaAssetPointLocationPointApSetParamSampleTime(customer, siteId, areaId, assetId, pointLocationId, pointId, apSetId, paramName, sampleTime)) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                    .append("{\"customerAccount\":\"").append(customer).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"assetId\":\"").append(assetId).append("\",")
                    .append("\"pointLocationId\":\"").append(pointLocationId).append("\",")
                    .append("\"pointId\":\"").append(pointId).append("\",")
                    .append("\"apSetId\":\"").append(apSetId).append("\",")
                    .append("\"paramName\":\"").append(paramName).append("\",")
                    .append("\"sampleTime\":\"").append(sampleTime).append("\",")
                    .append("\"TrendAcValues\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
            return json.toString();
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Retrieve a List of TrendAcValues Objects for the given customerAcct, siteId, areaId, assetId, pointLocationId, pointId, apSetId, paramName and sampleTime from trend_ac_values table
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @param pointLocationId, String Object
     * @param pointId, String Object
     * @param apSetId, String Object
     * @param paramName, String Object
     * @param startDate , String Object
     * @param endDate , String Object
     * @return String Object
     * @throws UnavailableException
     * @throws ReadTimeoutException
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String getByCustomerSiteAreaAssetPointLocationPointApSetParamDateRange(String customer, String siteId, String areaId, String assetId, String pointLocationId, String pointId, String apSetId, String paramName, String startDate, String endDate) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<TrendAcValues> result;
        StringBuilder json;
        
        if ((result = readDelegate.queryByDateRange(customer, siteId, areaId, assetId, pointLocationId, pointId, apSetId, paramName, startDate, endDate)) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                    .append("{\"customerAccount\":\"").append(customer).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"assetId\":\"").append(assetId).append("\",")
                    .append("\"pointLocationId\":\"").append(pointLocationId).append("\",")
                    .append("\"pointId\":\"").append(pointId).append("\",")
                    .append("\"apSetId\":\"").append(apSetId).append("\",")
                    .append("\"paramName\":\"").append(paramName).append("\",")
                    .append("\"startDate\":\"").append(startDate).append("\",")
                    .append("\"endDate\":\"").append(endDate).append("\",")
                    .append("\"TrendAcValues\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
            return json.toString();
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Retrieve a List of TrendDcValues Objects for the given customerAcct, siteId, areaId, assetId, pointLocationId, pointId, apSetId, paramName and sampleTime from trend_ac_values table
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @param pointLocationId, String Object
     * @param pointId, String Object
     * @param apSetId, String Object
     * @param startDate , String Object
     * @param endDate , String Object
     * @return String Object
     * @throws UnavailableException
     * @throws ReadTimeoutException
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String getByCustomerSiteAreaAssetPointLocationPointApSetDateRange(String customer, String siteId, String areaId, String assetId, String pointLocationId, String pointId, String apSetId, String startDate, String endDate) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<TrendDcValues> result;
        StringBuilder json;
        
        if ((result = readDelegate.queryByDateRange(customer, siteId, areaId, assetId, pointLocationId, pointId, apSetId, startDate, endDate)) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                    .append("{\"customerAccount\":\"").append(customer).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"assetId\":\"").append(assetId).append("\",")
                    .append("\"pointLocationId\":\"").append(pointLocationId).append("\",")
                    .append("\"pointId\":\"").append(pointId).append("\",")
                    .append("\"apSetId\":\"").append(apSetId).append("\",")
                    .append("\"startDate\":\"").append(startDate).append("\",")
                    .append("\"endDate\":\"").append(endDate).append("\",")
                    .append("\"TrendDcValues\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
            return json.toString();
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Retrieve a List of TrendAcValues Objects for the given customerAcct, siteId, areaId, assetId, pointLocationId, pointId, apSetId, paramName and limit from trend_ac_values table
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @param pointLocationId, String Object
     * @param pointId, String Object
     * @param apSetId, String Object
     * @param paramName, String Object
     * @param limit, String Object
     * @return String Object
     * @throws UnavailableException
     * @throws ReadTimeoutException
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String getByCustomerSiteAreaAssetPointLocationPointApSetParamLimit(String customer, String siteId, String areaId, String assetId, String pointLocationId, String pointId, String apSetId, String paramName, String limit) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<TrendAcValues> result;
        StringBuilder json;

        if ((result = readDelegate.getByCustomerSiteAreaAssetPointLocationPointApSetParamLimit(customer, siteId, areaId, assetId, pointLocationId, pointId, apSetId, paramName, limit)) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                    .append("{\"customerAccount\":\"").append(customer).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"assetId\":\"").append(assetId).append("\",")
                    .append("\"pointLocationId\":\"").append(pointLocationId).append("\",")
                    .append("\"pointId\":\"").append(pointId).append("\",")
                    .append("\"apSetId\":\"").append(apSetId).append("\",")
                    .append("\"paramName\":\"").append(paramName).append("\",")
                    .append("\"limit\":\"").append(limit).append("\",")
                    .append("\"TrendAcValues\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
            return json.toString();
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Retrieve a List of TrendDcValues Objects for the given customerAcct, siteId, areaId, assetId, pointLocationId, pointId, apSetId and sampleTime from trend_dc_values table
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @param pointLocationId, String Object
     * @param pointId, String Object
     * @param apSetId, String Object
     * @param sampleTime, String Object
     * @return String Object
     * @throws UnavailableException
     * @throws ReadTimeoutException
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String getByCustomerSiteAreaAssetPointLocationPointApSetSampleTime(String customer, String siteId, String areaId, String assetId, String pointLocationId, String pointId, String apSetId, String sampleTime) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<TrendDcValues> result;
        StringBuilder json;

        if ((result = readDelegate.getByCustomerSiteAreaAssetPointLocationPointApSetSampleTime(customer, siteId, areaId, assetId, pointLocationId, pointId, apSetId, sampleTime)) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                    .append("{\"customerAccount\":\"").append(customer).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"assetId\":\"").append(assetId).append("\",")
                    .append("\"pointLocationId\":\"").append(pointLocationId).append("\",")
                    .append("\"pointId\":\"").append(pointId).append("\",")
                    .append("\"apSetId\":\"").append(apSetId).append("\",")
                    .append("\"sampleTime\":\"").append(sampleTime).append("\",")
                    .append("\"TrendDcValues\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
            return json.toString();
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Retrieve a List of TrendDcValues Objects for the given customerAcct, siteId, areaId, assetId, pointLocationId, pointId, apSetId and limit from trend_dc_values table
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @param pointLocationId, String Object
     * @param pointId, String Object
     * @param apSetId, String Object
     * @param limit, String Object
     * @return String Object
     * @throws UnavailableException
     * @throws ReadTimeoutException
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String getByCustomerSiteAreaAssetPointLocationPointApSetLimit(String customer, String siteId, String areaId, String assetId, String pointLocationId, String pointId, String apSetId, String limit) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<TrendDcValues> result;
        StringBuilder json;

        if ((result = readDelegate.getByCustomerSiteAreaAssetPointLocationPointApSetLimit(customer, siteId, areaId, assetId, pointLocationId, pointId, apSetId, limit)) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                    .append("{\"customerAccount\":\"").append(customer).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"assetId\":\"").append(assetId).append("\",")
                    .append("\"pointLocationId\":\"").append(pointLocationId).append("\",")
                    .append("\"pointId\":\"").append(pointId).append("\",")
                    .append("\"apSetId\":\"").append(apSetId).append("\",")
                    .append("\"limit\":\"").append(limit).append("\",")
                    .append("\"TrendDcValues\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
            return json.toString();
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Create a new WaveFormData by inserting into trend_dc_values or trend_ac_values tables
     *
     * @param content, String Object
     * @return String Object
     */
    public String createTrendValues(String content) {
        CreateTrendDelegate createDelegate;
        WaveFormDataVO waveFormDataVO;
        
        if ((waveFormDataVO = JsonUtil.parser(content)) != null) {
            if (waveFormDataVO.getCustomerAccount() != null && !waveFormDataVO.getCustomerAccount().isEmpty()
                    && waveFormDataVO.getSiteId() != null
                    && waveFormDataVO.getAreaId() != null
                    && waveFormDataVO.getAssetId() != null
                    && waveFormDataVO.getPointLocationId() != null
                    && waveFormDataVO.getPointId() != null
                    && waveFormDataVO.getApSetId() != null
                    && waveFormDataVO.getSampleTime() != null) {

                // Insert into Cassandra
                try {
                    createDelegate = new CreateTrendDelegate();
                    return (waveFormDataVO.getParamName() != null && !waveFormDataVO.getParamName().isEmpty()) ? createDelegate.createTrendAcValues(waveFormDataVO) : createDelegate.createTrendDcValues(waveFormDataVO);
                } catch (IllegalArgumentException e) {
                    return "{\"outcome\":\"Error: Failed to create new Trend do to IllegalArgumentException.\"}";
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                    return "{\"outcome\":\"Error: Failed to create new Trend.\"}";
                }

            } else {
                return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Update tables dealing with the given Trend values
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String updateTrendValues(String content) throws IllegalArgumentException {
        WaveFormDataVO waveFormDataVO;
        UpdateTrendDelegate updateDelegate;
        
        if ((waveFormDataVO = JsonUtil.parser(content)) != null) {
            if (waveFormDataVO.getCustomerAccount() != null && !waveFormDataVO.getCustomerAccount().isEmpty()
                    && waveFormDataVO.getSiteId() != null
                    && waveFormDataVO.getAreaId() != null
                    && waveFormDataVO.getAssetId() != null
                    && waveFormDataVO.getPointLocationId() != null
                    && waveFormDataVO.getPointId() != null
                    && waveFormDataVO.getApSetId() != null
                    && waveFormDataVO.getAlSetId() != null
                    && waveFormDataVO.getSampleTime() != null) {
                
                // Update in Cassandra
                try {
                    updateDelegate = new UpdateTrendDelegate();
                    return (waveFormDataVO.getParamName() != null && !waveFormDataVO.getParamName().isEmpty()) ? updateDelegate.updateTrendAcValues(waveFormDataVO) : updateDelegate.updateTrendDcValues(waveFormDataVO);
                } catch (IllegalArgumentException e) {
                    throw e;
                }
            } else {
                return "{\"outcome\":\"insufficient data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Delete tables dealing with the given Trend Values
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String deleteTrendValues(String content) throws IllegalArgumentException {
        DeleteTrendDelegate deleteDelegate;
        WaveFormDataVO waveFormDataVO;

        if ((waveFormDataVO = JsonUtil.parser(content)) != null) {
            if (waveFormDataVO.getCustomerAccount() != null && !waveFormDataVO.getCustomerAccount().isEmpty()
                    && waveFormDataVO.getSiteId() != null
                    && waveFormDataVO.getAreaId() != null
                    && waveFormDataVO.getAssetId() != null
                    && waveFormDataVO.getPointLocationId() != null
                    && waveFormDataVO.getPointId() != null
                    && waveFormDataVO.getApSetId() != null
                    && waveFormDataVO.getAlSetId() != null
                    && waveFormDataVO.getSampleTime() != null) {

                // Delete in Cassandra
                try {
                    deleteDelegate = new DeleteTrendDelegate();
                    return (waveFormDataVO.getParamName() != null && !waveFormDataVO.getParamName().isEmpty()) ? deleteDelegate.deleteTrendAcValues(waveFormDataVO) : deleteDelegate.deleteTrendDcValues(waveFormDataVO);
                } catch (IllegalArgumentException e) {
                    throw e;
                }

            } else {
                return "{\"outcome\":\"insufficient data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

}
