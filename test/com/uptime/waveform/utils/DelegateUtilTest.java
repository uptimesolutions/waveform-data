/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.waveform.utils;

import com.uptime.cassandra.trend.entity.TrendAcValues;
import com.uptime.cassandra.trend.entity.TrendDcValues;
import com.uptime.waveform.vo.WaveFormDataVO;
import java.time.Instant;
import java.util.UUID;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Mohd Juned Alam
 */
public class DelegateUtilTest {

    private static final WaveFormDataVO waveFormDataVO = new WaveFormDataVO();

    /**
     * Constructor
     */
    public DelegateUtilTest() {
        
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
        waveFormDataVO.setCustomerAccount("77777");
        waveFormDataVO.setSiteId(UUID.randomUUID());
        waveFormDataVO.setAreaId(UUID.randomUUID());
        waveFormDataVO.setAssetId(UUID.randomUUID());
        waveFormDataVO.setPointLocationId(UUID.randomUUID());
        waveFormDataVO.setPointId(UUID.randomUUID());
        waveFormDataVO.setApSetId(UUID.randomUUID());
        waveFormDataVO.setAlSetId(UUID.randomUUID());
        waveFormDataVO.setParamName("Test Param");
        waveFormDataVO.setLabel("TEST TREND LABEL 22");
        waveFormDataVO.setUnits("TEST UNITS 1");
        waveFormDataVO.setValue(5f);
        waveFormDataVO.setSampleTime(Instant.now());
        waveFormDataVO.setNotes("Test Notes");
        waveFormDataVO.setValue(1.2f);
        waveFormDataVO.setHighAlert(5f);
        waveFormDataVO.setHighFault(4f);
        waveFormDataVO.setLowAlert(3f);
        waveFormDataVO.setLowFault(2f);
        waveFormDataVO.setSensorType("Test");
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test of getTrendAcValues method, of class DelegateUtil.
     */
    @Test
    public void test1_GetTrendAcValues() {
        System.out.println("TrendAcValues method testing.");
        TrendAcValues result = DelegateUtil.getTrendAcValues(waveFormDataVO);
        assertEquals(waveFormDataVO.getCustomerAccount(), result.getCustomerAccount());
        assertEquals(waveFormDataVO.getSiteId(), result.getSiteId());
        assertEquals(waveFormDataVO.getAreaId(), result.getAreaId());
        assertEquals(waveFormDataVO.getAssetId(), result.getAssetId());
        assertEquals(waveFormDataVO.getPointLocationId(), result.getPointLocationId());
        assertEquals(waveFormDataVO.getPointId(), result.getPointId());
        assertEquals(waveFormDataVO.getApSetId(), result.getApSetId());
        assertEquals(waveFormDataVO.getAlSetId(), result.getAlSetId());
        assertEquals(waveFormDataVO.getParamName(), result.getParamName());
        assertEquals(waveFormDataVO.getSampleTime(), result.getSampleTime());
        System.out.println("testGetTrendCustomerPoint method completed.");
    }

     /**
     * Test of getTrendDcValues method, of class DelegateUtil.
     */
    @Test
    public void testGetTrendDcValues() {
        System.out.println("getTrendDcValues");
        TrendDcValues result = DelegateUtil.getTrendDcValues(waveFormDataVO);
        assertEquals(waveFormDataVO.getCustomerAccount(), result.getCustomerAccount());
        assertEquals(waveFormDataVO.getSiteId(), result.getSiteId());
        assertEquals(waveFormDataVO.getAreaId(), result.getAreaId());
        assertEquals(waveFormDataVO.getAssetId(), result.getAssetId());
        assertEquals(waveFormDataVO.getPointLocationId(), result.getPointLocationId());
        assertEquals(waveFormDataVO.getPointId(), result.getPointId());
        assertEquals(waveFormDataVO.getApSetId(), result.getApSetId());
        assertEquals(waveFormDataVO.getAlSetId(), result.getAlSetId());
        assertEquals(waveFormDataVO.getSampleTime(), result.getSampleTime());
    }

}
