/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.waveform.controller;

import com.uptime.waveform.vo.WaveFormDataVO;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TrendControllerTest {

    static TrendController instance;

    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//waveformdataservice.cql", true, true, "worldview_dev1"));
    private static final WaveFormDataVO waveformdataVo = new WaveFormDataVO();
    
    @BeforeClass
    public static void setUpClass() {
        instance = new TrendController();
        
        waveformdataVo.setSampleTime(Instant.parse("2022-02-01T01:00:00Z").atZone(ZoneOffset.UTC).toInstant());
        waveformdataVo.setCustomerAccount("77777");
        waveformdataVo.setSiteId(UUID.fromString("dee5158a-30b0-4c12-b6b9-40cb91d1206d"));
        waveformdataVo.setAreaId(UUID.fromString("98d2cfb9-069e-4dd7-8ac0-f2835351e5b0"));
        waveformdataVo.setAssetId(UUID.fromString("b48a318e-352c-4c6e-a3e2-4ad7756681ce"));
        waveformdataVo.setPointLocationId(UUID.fromString("1485cbd3-28a7-4f21-80c4-f3ddb894465e"));
        waveformdataVo.setPointId(UUID.fromString("013c7746-16cf-4ae5-a124-87dd41c87f79"));
        waveformdataVo.setApSetId(UUID.fromString("d623b71a-6d3b-49dd-bd51-d5b0647cfc5f"));
        waveformdataVo.setAlSetId(UUID.fromString("79c2e5b9-7328-4fcf-aa4c-fba233481152"));
        waveformdataVo.setParamName("Test Param 1");
        waveformdataVo.setLabel("Test Label 1");
        waveformdataVo.setNotes("Test Notes 1");
        waveformdataVo.setUnits("Test Units 1");
        waveformdataVo.setSensorType("Test Sensor Type 1");
        waveformdataVo.setValue(5f);
        waveformdataVo.setHighAlert((float) 0.145);
        waveformdataVo.setHighFault((float) 0.146);
        waveformdataVo.setLowAlert((float) 0.147);
        waveformdataVo.setLowFault((float) 0.148);
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test of createTrendValues method, of class
 TrendController.
     */
    @Test
    public void test1_CreateTrendACValues() {
        System.out.println("CreateTrendACValues method testing.");
        String inputJson = "{\n" +
"    \"customerAccount\": \"77777\",\n" +
"    \"siteId\": \"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\n" +
"    \"areaId\": \"98d2cfb9-069e-4dd7-8ac0-f2835351e5b0\",\n" +
"    \"assetId\": \"b48a318e-352c-4c6e-a3e2-4ad7756681ce\",\n" +
"    \"pointLocationId\": \"1485cbd3-28a7-4f21-80c4-f3ddb894465e\",\n" +
"    \"pointId\": \"013c7746-16cf-4ae5-a124-87dd41c87f79\",\n" +
"    \"apSetId\": \"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\n" +
"    \"alSetId\": \"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\n" +
"    \"paramName\": \"Test Param 1\",\n" +
"    \"sampleTime\": \"2022-02-01T01:00:00Z\",\n" +
"    \"label\": \"Test Label 1\",\n" +
"    \"units\": \"Test Units 1\",\n" +
"    \"sensorType\": \"Test Sensor Type 1\",\n" +
"    \"notes\": \"Test Notes 1\",\n" +
"    \"highAlert\": 0.145,\n" +
"    \"highFault\": 0.146,\n" +
"    \"lowAlert\": 0.147,\n" +
"    \"lowFault\": 0.148,\n" +
"    \"value\": 5\n" +
"}";
        String inputJsonFail = "{\"customerAccount\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"areaId\":\"98d2cfb9-069e-4dd7-8ac0-f2835351e5b0\",\"pointLocationId\":\"1485cbd3-28a7-4f21-80c4-f3ddb894465e\",\"pointId\":\"013c7746-16cf-4ae5-a124-87dd41c87f79\",\"apSetId\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\"alSetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\"paramName\":\"" + waveformdataVo.getParamName() + "\",\"sampleTime\":\"" + waveformdataVo.getSampleTime()+ "\",\"label\":\"TEST TREND LABEL 1\",\"alarmLimit\":\"5\",\"units\":\"TEST UNITS 1\",\"value\":\"5\"}";
        System.out.println("inputJson - " + inputJson);
        String result = instance.createTrendValues(inputJson);
        System.out.println("result - " + result);
        assertEquals("{\"outcome\":\"New AC Trend Values created successfully.\"}", result);
        assertEquals("{\"outcome\":\"insufficient and/or invalid data given in json\"}", instance.createTrendValues(inputJsonFail));
        assertEquals("{\"outcome\":\"Json is invalid\"}", instance.createTrendValues(null));
        System.out.println("CreateTrendACValues method complete.");
    }
    
    /**
     * Test of CreateTrendValues method, of class
 TrendController.
     */
    @Test
    public void test2_CreateDCTrendValues() {
        System.out.println("CreateDCTrendValues method testing.");
        String inputJson = "{\n" +
"    \"customerAccount\": \"77777\",\n" +
"    \"siteId\": \"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\n" +
"    \"areaId\": \"98d2cfb9-069e-4dd7-8ac0-f2835351e5b0\",\n" +
"    \"assetId\": \"b48a318e-352c-4c6e-a3e2-4ad7756681ce\",\n" +
"    \"pointLocationId\": \"1485cbd3-28a7-4f21-80c4-f3ddb894465e\",\n" +
"    \"pointId\": \"013c7746-16cf-4ae5-a124-87dd41c87f79\",\n" +
"    \"apSetId\": \"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\n" +
"    \"alSetId\": \"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\n" +
"    \"label\": \"Test Label 1\",\n" +
"    \"units\": \"Test Units 1\",\n" +
"    \"sensorType\": \"Test Sensor Type 1\",\n" +
"    \"sampleTime\" : \"2022-02-01T01:00:00Z\",\n" +                
"    \"notes\": \"Test Notes 1\",\n" +
"    \"highAlert\": 0.145,\n" +
"    \"highFault\": 0.146,\n" +
"    \"lowAlert\": 0.147,\n" +
"    \"lowFault\": 0.148,\n" +
"    \"value\": 5\n" +
"}";
        String inputJsonFail = "{\"customerAccount\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"areaId\":\"98d2cfb9-069e-4dd7-8ac0-f2835351e5b0\",\"pointLocationId\":\"1485cbd3-28a7-4f21-80c4-f3ddb894465e\",\"pointId\":\"013c7746-16cf-4ae5-a124-87dd41c87f79\",\"alSetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\"paramName\":\"" + waveformdataVo.getParamName() + "\",\"sampleTime\":\"" + waveformdataVo.getSampleTime() + "\",\"label\":\"Test Label 1\",\"alarmLimit\":\"5\",\"units\":\"TEST UNITS 1\",\"value\":\"5\"}";
        System.out.println("inputJson - " + inputJson);
        assertEquals("{\"outcome\":\"New DC Trend Values created successfully.\"}", instance.createTrendValues(inputJson));
        assertEquals("{\"outcome\":\"insufficient and/or invalid data given in json\"}", instance.createTrendValues(inputJsonFail));
        assertEquals("{\"outcome\":\"Json is invalid\"}", instance.createTrendValues(null));
        System.out.println("CreateDCTrendValues method complete.");
    }

    /**
     * Test of getByCustomerSiteAreaAssetPointLocationPointApSetParamRow method, of class TrendController.
     */
    @Test
    public void test3_GetByCustomerSiteAreaAssetPointLocationPointApSetParamSampleTime() throws Exception {
        System.out.println("getByCustomerSiteAreaAssetPointLocationPointApSetParamSampleTime");
        String expResult = "{\"customerAccount\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"areaId\":\"98d2cfb9-069e-4dd7-8ac0-f2835351e5b0\",\"assetId\":\"b48a318e-352c-4c6e-a3e2-4ad7756681ce\",\"pointLocationId\":\"1485cbd3-28a7-4f21-80c4-f3ddb894465e\",\"pointId\":\"013c7746-16cf-4ae5-a124-87dd41c87f79\",\"apSetId\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\"paramName\":\"Test Param 1\",\"sampleTime\":\"2022-02-01T01:00:00Z\",\"TrendAcValues\":[{\"customerAccount\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"areaId\":\"98d2cfb9-069e-4dd7-8ac0-f2835351e5b0\",\"assetId\":\"b48a318e-352c-4c6e-a3e2-4ad7756681ce\",\"pointLocationId\":\"1485cbd3-28a7-4f21-80c4-f3ddb894465e\",\"pointId\":\"013c7746-16cf-4ae5-a124-87dd41c87f79\",\"apSetId\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\"paramName\":\"Test Param 1\",\"sampleTime\":\"2022-02-01T01:00:00+0000\",\"alSetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\"label\":\"Test Label 1\",\"notes\":\"Test Notes 1\",\"highAlert\":0.145,\"highFault\":0.146,\"lowAlert\":0.147,\"lowFault\":0.148,\"sensorType\":\"Test Sensor Type 1\",\"units\":\"Test Units 1\",\"value\":5.0}],\"outcome\":\"GET worked successfully.\"}";
        System.out.println("waveformdataVo.getSampleTime().toString() - " + waveformdataVo.getSampleTime().toString());
        String result = instance.getByCustomerSiteAreaAssetPointLocationPointApSetParamSampleTime(waveformdataVo.getCustomerAccount(), waveformdataVo.getSiteId().toString(), waveformdataVo.getAreaId().toString(), waveformdataVo.getAssetId().toString(), waveformdataVo.getPointLocationId().toString(), waveformdataVo.getPointId().toString(), waveformdataVo.getApSetId().toString(), waveformdataVo.getParamName(), waveformdataVo.getSampleTime().toString());
        System.out.println("test3 expResult - " + expResult);
        System.out.println("test3 result - " + result);
        assertEquals(expResult, result);

    }

    /**
     * Test of getByCustomerSiteAreaAssetPointLocationPointApSetParam method, of class TrendController.
     */
    @Test
    public void test4_GetByCustomerSiteAreaAssetPointLocationPointApSetParamLimit() throws Exception {
        System.out.println("getByCustomerSiteAreaAssetPointLocationPointApSetParamLimit");
        String expResult = "{\"customerAccount\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"areaId\":\"98d2cfb9-069e-4dd7-8ac0-f2835351e5b0\",\"assetId\":\"b48a318e-352c-4c6e-a3e2-4ad7756681ce\",\"pointLocationId\":\"1485cbd3-28a7-4f21-80c4-f3ddb894465e\",\"pointId\":\"013c7746-16cf-4ae5-a124-87dd41c87f79\",\"apSetId\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\"paramName\":\"Test Param 1\",\"limit\":\"1\",\"TrendAcValues\":[{\"customerAccount\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"areaId\":\"98d2cfb9-069e-4dd7-8ac0-f2835351e5b0\",\"assetId\":\"b48a318e-352c-4c6e-a3e2-4ad7756681ce\",\"pointLocationId\":\"1485cbd3-28a7-4f21-80c4-f3ddb894465e\",\"pointId\":\"013c7746-16cf-4ae5-a124-87dd41c87f79\",\"apSetId\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\"paramName\":\"Test Param 1\",\"sampleTime\":\"2022-02-01T01:00:00+0000\",\"alSetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\"label\":\"Test Label 1\",\"notes\":\"Test Notes 1\",\"highAlert\":0.145,\"highFault\":0.146,\"lowAlert\":0.147,\"lowFault\":0.148,\"sensorType\":\"Test Sensor Type 1\",\"units\":\"Test Units 1\",\"value\":5.0}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByCustomerSiteAreaAssetPointLocationPointApSetParamLimit(waveformdataVo.getCustomerAccount(), waveformdataVo.getSiteId().toString(), waveformdataVo.getAreaId().toString(), waveformdataVo.getAssetId().toString(), waveformdataVo.getPointLocationId().toString(), waveformdataVo.getPointId().toString(), waveformdataVo.getApSetId().toString(), waveformdataVo.getParamName(), "1");
        System.out.println("test4 expResult - " + expResult);
        System.out.println("test4 result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByCustomerSiteAreaAssetPointLocationPointApSetRow method, of class TrendController.
     */
    @Test
    public void test5_GetByCustomerSiteAreaAssetPointLocationPointApSetSampleTime() throws Exception {
        System.out.println("getByCustomerSiteAreaAssetPointLocationPointApSetSampleTime");
        String expResult = "{\"customerAccount\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"areaId\":\"98d2cfb9-069e-4dd7-8ac0-f2835351e5b0\",\"assetId\":\"b48a318e-352c-4c6e-a3e2-4ad7756681ce\",\"pointLocationId\":\"1485cbd3-28a7-4f21-80c4-f3ddb894465e\",\"pointId\":\"013c7746-16cf-4ae5-a124-87dd41c87f79\",\"apSetId\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\"sampleTime\":\"2022-02-01T01:00:00Z\",\"TrendDcValues\":[{\"customerAccount\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"areaId\":\"98d2cfb9-069e-4dd7-8ac0-f2835351e5b0\",\"assetId\":\"b48a318e-352c-4c6e-a3e2-4ad7756681ce\",\"pointLocationId\":\"1485cbd3-28a7-4f21-80c4-f3ddb894465e\",\"pointId\":\"013c7746-16cf-4ae5-a124-87dd41c87f79\",\"apSetId\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\"sampleTime\":\"2022-02-01T01:00:00+0000\",\"alSetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\"label\":\"Test Label 1\",\"units\":\"Test Units 1\",\"notes\":\"Test Notes 1\",\"value\":5.0,\"highAlert\":0.145,\"highFault\":0.146,\"lowAlert\":0.147,\"lowFault\":0.148,\"sensorType\":\"Test Sensor Type 1\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByCustomerSiteAreaAssetPointLocationPointApSetSampleTime(waveformdataVo.getCustomerAccount(), waveformdataVo.getSiteId().toString(), waveformdataVo.getAreaId().toString(), waveformdataVo.getAssetId().toString(), waveformdataVo.getPointLocationId().toString(), waveformdataVo.getPointId().toString(), waveformdataVo.getApSetId().toString(), waveformdataVo.getSampleTime().toString());
        System.out.println("test5 expResult - " + expResult);
        System.out.println("test5 result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByCustomerSiteAreaAssetPointLocationPointApSet method, of class TrendController.
     */
    @Test
    public void test6_GetByCustomerSiteAreaAssetPointLocationPointApSetLimit() throws Exception {
        System.out.println("getByCustomerSiteAreaAssetPointLocationPointApSetLimit");
        String expResult = "{\"customerAccount\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"areaId\":\"98d2cfb9-069e-4dd7-8ac0-f2835351e5b0\",\"assetId\":\"b48a318e-352c-4c6e-a3e2-4ad7756681ce\",\"pointLocationId\":\"1485cbd3-28a7-4f21-80c4-f3ddb894465e\",\"pointId\":\"013c7746-16cf-4ae5-a124-87dd41c87f79\",\"apSetId\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\"limit\":\"1\",\"TrendDcValues\":[{\"customerAccount\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"areaId\":\"98d2cfb9-069e-4dd7-8ac0-f2835351e5b0\",\"assetId\":\"b48a318e-352c-4c6e-a3e2-4ad7756681ce\",\"pointLocationId\":\"1485cbd3-28a7-4f21-80c4-f3ddb894465e\",\"pointId\":\"013c7746-16cf-4ae5-a124-87dd41c87f79\",\"apSetId\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\"sampleTime\":\"2022-02-01T01:00:00+0000\",\"alSetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\"label\":\"Test Label 1\",\"units\":\"Test Units 1\",\"notes\":\"Test Notes 1\",\"value\":5.0,\"highAlert\":0.145,\"highFault\":0.146,\"lowAlert\":0.147,\"lowFault\":0.148,\"sensorType\":\"Test Sensor Type 1\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByCustomerSiteAreaAssetPointLocationPointApSetLimit(waveformdataVo.getCustomerAccount(), waveformdataVo.getSiteId().toString(), waveformdataVo.getAreaId().toString(), waveformdataVo.getAssetId().toString(), waveformdataVo.getPointLocationId().toString(), waveformdataVo.getPointId().toString(), waveformdataVo.getApSetId().toString(), "1");
        System.out.println("test6 expResult - " + expResult);
        System.out.println("test6 result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateTrendValues method, of class TrendController.
     */
    @Test
    public void test7_UpdateTrendValues() {
        System.out.println("updateTrendValues");
        String content = "{\n" +
"    \"customerAccount\": \"77777\",\n" +
"    \"siteId\": \"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\n" +
"    \"areaId\": \"98d2cfb9-069e-4dd7-8ac0-f2835351e5b0\",\n" +
"    \"assetId\": \"b48a318e-352c-4c6e-a3e2-4ad7756681ce\",\n" +
"    \"pointLocationId\": \"1485cbd3-28a7-4f21-80c4-f3ddb894465e\",\n" +
"    \"pointId\": \"013c7746-16cf-4ae5-a124-87dd41c87f79\",\n" +
"    \"apSetId\": \"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\n" +
"    \"alSetId\": \"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\n" +
"    \"paramName\": \"Test Param 1\",\n" +
"    \"sampleTime\": \"2022-02-01T01:00:00Z\",\n" +
"    \"label\": \"Test Label 1\",\n" +
"    \"units\": \"Test Units 1\",\n" +
"    \"sensorType\": \"Test Sensor Type 1\",\n" +
"    \"notes\": \"Test Notes 1\",\n" +
"    \"highAlert\": 0.145,\n" +
"    \"highFault\": 0.146,\n" +
"    \"lowAlert\": 0.147,\n" +
"    \"lowFault\": 0.148,\n" +
"    \"value\": 10\n" +
"}";
        String expResult = "{\"outcome\":\"Updated AC Trend Values successfully.\"}";
        String result = instance.updateTrendValues(content);
        System.out.println("test7 result - " + result);
        assertEquals(expResult, result);
    }
    /**
     * Test of updateTrendValues method, of class TrendController.
     */
    @Test
    public void test8_UpdateTrendValues() {
        System.out.println("updateTrendValues");
        String content = "{\n" +
"    \"customerAccount\": \"77777\",\n" +
"    \"siteId\": \"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\n" +
"    \"areaId\": \"98d2cfb9-069e-4dd7-8ac0-f2835351e5b0\",\n" +
"    \"assetId\": \"b48a318e-352c-4c6e-a3e2-4ad7756681ce\",\n" +
"    \"pointLocationId\": \"1485cbd3-28a7-4f21-80c4-f3ddb894465e\",\n" +
"    \"pointId\": \"013c7746-16cf-4ae5-a124-87dd41c87f79\",\n" +
"    \"apSetId\": \"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\n" +
"    \"alSetId\": \"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\n" +
"    \"label\": \"Test Label 1\",\n" +
"    \"units\": \"Test Units 1\",\n" +
"    \"sensorType\": \"Test Sensor Type 1\",\n" +
"    \"sampleTime\" : \"2022-02-01T01:00:00Z\",\n" +                
"    \"notes\": \"Test Notes 1\",\n" +
"    \"highAlert\": 0.145,\n" +
"    \"highFault\": 0.146,\n" +
"    \"lowAlert\": 0.147,\n" +
"    \"lowFault\": 0.148,\n" +
"    \"value\": 15\n" +
"}";
        String expResult = "{\"outcome\":\"Updated DC Trend Values successfully.\"}";
        String result = instance.updateTrendValues(content);
        System.out.println("test8 result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteTrendValues method, of class TrendController.
     */
    @Test
    public void test9_DeleteTrendValues() {
        System.out.println("deleteTrendValues");
        String content = "{\n" +
"    \"customerAccount\": \"77777\",\n" +
"    \"siteId\": \"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\n" +
"    \"areaId\": \"98d2cfb9-069e-4dd7-8ac0-f2835351e5b0\",\n" +
"    \"assetId\": \"b48a318e-352c-4c6e-a3e2-4ad7756681ce\",\n" +
"    \"pointLocationId\": \"1485cbd3-28a7-4f21-80c4-f3ddb894465e\",\n" +
"    \"pointId\": \"013c7746-16cf-4ae5-a124-87dd41c87f79\",\n" +
"    \"apSetId\": \"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\n" +
"    \"alSetId\": \"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\n" +
"    \"paramName\": \"Test Param 1\",\n" +
"    \"sampleTime\": \"2022-02-01T01:00:00Z\",\n" +
"    \"label\": \"Test Label 1\",\n" +
"    \"units\": \"Test Units 1\",\n" +
"    \"sensorType\": \"Test Sensor Type 1\",\n" +
"    \"notes\": \"Test Notes 1\",\n" +
"    \"highAlert\": 0.145,\n" +
"    \"highFault\": 0.146,\n" +
"    \"lowAlert\": 0.147,\n" +
"    \"lowFault\": 0.148,\n" +
"    \"value\": 5\n" +
"}";
        String expResult = "{\"outcome\":\"Deleted AC Trend Values successfully.\"}";
        String result = instance.deleteTrendValues(content);
        System.out.println("test9 result - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of deleteTrendValues method, of class TrendController.
     */
    @Test
    public void test91_DeleteTrendValues() {
        System.out.println("deleteTrendValues");
        String content = "{\n" +
"    \"customerAccount\": \"77777\",\n" +
"    \"siteId\": \"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\n" +
"    \"areaId\": \"98d2cfb9-069e-4dd7-8ac0-f2835351e5b0\",\n" +
"    \"assetId\": \"b48a318e-352c-4c6e-a3e2-4ad7756681ce\",\n" +
"    \"pointLocationId\": \"1485cbd3-28a7-4f21-80c4-f3ddb894465e\",\n" +
"    \"pointId\": \"013c7746-16cf-4ae5-a124-87dd41c87f79\",\n" +
"    \"apSetId\": \"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\n" +
"    \"alSetId\": \"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\n" +
"    \"label\": \"Test Label 1\",\n" +
"    \"units\": \"Test Units 1\",\n" +
"    \"sensorType\": \"Test Sensor Type 1\",\n" +
"    \"sampleTime\" : \"2022-02-01T01:00:00Z\",\n" +                
"    \"notes\": \"Test Notes 1\",\n" +
"    \"highAlert\": 0.145,\n" +
"    \"highFault\": 0.146,\n" +
"    \"lowAlert\": 0.147,\n" +
"    \"lowFault\": 0.148,\n" +
"    \"value\": 5\n" +
"}";
        String expResult = "{\"outcome\":\"Deleted DC Trend Values successfully.\"}";
        String result = instance.deleteTrendValues(content);
        System.out.println("test91 result - " + result);
        assertEquals(expResult, result);
    }
}
