/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.waveform.controller;

import com.uptime.cassandra.trend.entity.Waveform;
import com.uptime.waveform.vo.WaveFormDataVO;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class WaveDataControllerTest {

    static WaveDataController instance;

    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//waveformdataservice.cql", true, true, "worldview_dev1"));
    private static final WaveFormDataVO waveformdataVo = new WaveFormDataVO();
    
    @BeforeClass
    public static void setUpClass() {
        instance = new WaveDataController();
        waveformdataVo.setSampleTime(Instant.parse("2022-08-01T01:00:00Z").atZone(ZoneOffset.UTC).toInstant());
        waveformdataVo.setCustomerAccount("77777");
        waveformdataVo.setSiteId(UUID.fromString("dee5158a-30b0-4c12-b6b9-40cb91d1206d"));
        waveformdataVo.setAreaId(UUID.fromString("98d2cfb9-069e-4dd7-8ac0-f2835351e5b0"));
        waveformdataVo.setAssetId(UUID.fromString("b48a318e-352c-4c6e-a3e2-4ad7756681ce"));
        waveformdataVo.setPointLocationId(UUID.fromString("1485cbd3-28a7-4f21-80c4-f3ddb894465e"));
        waveformdataVo.setPointId(UUID.fromString("013c7746-16cf-4ae5-a124-87dd41c87f79"));
        waveformdataVo.setSampleYear((short) 2022);
        waveformdataVo.setSampleMonth((byte)8);
        waveformdataVo.setSensitivityValue(0.9F);
        
        Waveform wf = new Waveform();
        wf.setChannelNum((byte)1);
        wf.setDeviceId("Test Device Id");
        wf.setSampleRateHz(0.9F);
        wf.setSampleTime(Instant.parse("2022-08-01T01:00:00Z").atZone(ZoneOffset.UTC).toInstant());
        wf.setSensitivityUnits("Test Sensitivity Units");
        wf.setTachSpeed(12.1f);
        List<Double> samples = new ArrayList<>();
        samples.add(12.2);
        samples.add(12.3);
        samples.add(12.4);
        wf.setSamples(samples);
        waveformdataVo.setWaveform(wf);
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test of CreateWaveForm method, of class WaveDataController.
     */
    @Test
    public void test10_CreateWaveForm() {
        System.out.println("CreateWaveForm method testing.");
        String inputJson = "{\n" +
"    \"customerAccount\": \"77777\",\n" +
"    \"siteId\": \"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\n" +
"    \"areaId\": \"98d2cfb9-069e-4dd7-8ac0-f2835351e5b0\",\n" +
"    \"assetId\": \"b48a318e-352c-4c6e-a3e2-4ad7756681ce\",\n" +
"    \"pointLocationId\": \"1485cbd3-28a7-4f21-80c4-f3ddb894465e\",\n" +
"    \"pointId\": \"013c7746-16cf-4ae5-a124-87dd41c87f79\",\n" +
"    \"sampleYear\": \"2022\",\n" +
"    \"sampleMonth\": \"8\",\n" +
"    \"sampleTime\": \"2022-08-01T01:00:00Z\",\n" +
"    \"sensitivityValue\": 0.148,\n" +
"    \"waveform\": {\"sampleTime\" : \"2022-08-01T01:00:00Z\",\n" +
            "\"deviceId\" : \"Test Device Id 1\",\n" +
            "\"channelNum\" : \"1\",\n" +
            "\"sampleRateHz\" : \"0.456\",\n" +
            "\"sensitivityUnits\" : \"Hz\",\n" +
            "\"tachSpeed\" : \"12.1f\",\n" +
            "\"samples\" : [1.23, 1.24, 1.25,1.26]"
                + "}\n" +
"}";
        String inputJsonFail = "{\"customerAccount\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"areaId\":\"98d2cfb9-069e-4dd7-8ac0-f2835351e5b0\",\"pointLocationId\":\"1485cbd3-28a7-4f21-80c4-f3ddb894465e\",\"pointId\":\"013c7746-16cf-4ae5-a124-87dd41c87f79\",\"apSetId\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\"alSetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\"paramName\":\"" + waveformdataVo.getParamName() + "\",\"sampleTime\":\"" + waveformdataVo.getSampleTime()+ "\",\"label\":\"TEST TREND LABEL 1\",\"alarmLimit\":\"5\",\"units\":\"TEST UNITS 1\",\"value\":\"5\"}";
        System.out.println("inputJson - " + inputJson);
        String result = instance.createWaveData(inputJson);
        System.out.println("result - " + result);
        assertEquals("{\"outcome\":\"New WaveData created successfully.\"}", result);
        assertEquals("{\"outcome\":\"insufficient and/or invalid data given in json\"}", instance.createWaveData(inputJsonFail));
        assertEquals("{\"outcome\":\"Json is invalid\"}", instance.createWaveData(null));
        System.out.println("CreateTrendACValues method complete.");
    }

    /**
     * Test of CreateWaveForm method, of class WaveDataController.
     */
    @Test
    public void test11_CreateWaveForm() {
        System.out.println("CreateWaveForm method testing.");
        String inputJson = "{\n" +
"    \"customerAccount\": \"77777\",\n" +
"    \"siteId\": \"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\n" +
"    \"areaId\": \"98d2cfb9-069e-4dd7-8ac0-f2835351e5b0\",\n" +
"    \"assetId\": \"b48a318e-352c-4c6e-a3e2-4ad7756681ce\",\n" +
"    \"pointLocationId\": \"1485cbd3-28a7-4f21-80c4-f3ddb894465e\",\n" +
"    \"pointId\": \"013c7746-16cf-4ae5-a124-87dd41c87f79\",\n" +
"    \"sampleYear\": \"2022\",\n" +
"    \"sampleMonth\": \"8\",\n" +
"    \"sampleTime\": \"2022-11-16T03:00:00Z\",\n" +
"    \"sensitivityValue\": 0.148,\n" +
    "\"deviceId\" : \"Test Device Id 1\",\n" +
    "\"channelNum\" : \"1\",\n" +
    "\"sampleRateInHz\" : \"0.456\",\n" +
    "\"sensitivityUnits\" : \"Hz\",\n" +
    "\"tachSpeed\" : \"12.1f\",\n" +
    "\"waveData\" : [1.23, 1.24, 1.25,1.26]"
                + "\n" +
"}";
        String inputJsonFail = "{\"customerAccount\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"areaId\":\"98d2cfb9-069e-4dd7-8ac0-f2835351e5b0\",\"pointLocationId\":\"1485cbd3-28a7-4f21-80c4-f3ddb894465e\",\"pointId\":\"013c7746-16cf-4ae5-a124-87dd41c87f79\",\"apSetId\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\"alSetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\"paramName\":\"" + waveformdataVo.getParamName() + "\",\"sampleTime\":\"" + waveformdataVo.getSampleTime()+ "\",\"label\":\"TEST TREND LABEL 1\",\"alarmLimit\":\"5\",\"units\":\"TEST UNITS 1\",\"value\":\"5\"}";
        System.out.println("inputJson - " + inputJson);
        String result = instance.createWaveData(inputJson);
        System.out.println("result - " + result);
        assertEquals("{\"outcome\":\"New WaveData created successfully.\"}", result);
        assertEquals("{\"outcome\":\"insufficient and/or invalid data given in json\"}", instance.createWaveData(inputJsonFail));
        assertEquals("{\"outcome\":\"Json is invalid\"}", instance.createWaveData(null));
        System.out.println("createWaveData method complete.");
    }

    /**
     * Test of getByCustomerSiteAreaAssetPointLocationPointApSetParamRow method, of class WaveDataController.
     */
    @Test
    public void test20_GetByCustomerSiteAreaAssetPointLocationPointsampleYearsampleMonthSampleTime() throws Exception {
        System.out.println("getByCustomerSiteAreaAssetPointLocationPointsampleYearsampleMonthSampleTime");
        String expResult = "{\"waveData\":{\"customerAccount\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"areaId\":\"98d2cfb9-069e-4dd7-8ac0-f2835351e5b0\",\"assetId\":\"b48a318e-352c-4c6e-a3e2-4ad7756681ce\",\"pointLocationId\":\"1485cbd3-28a7-4f21-80c4-f3ddb894465e\",\"pointId\":\"013c7746-16cf-4ae5-a124-87dd41c87f79\",\"sampleYear\":2022,\"sampleMonth\":8,\"sampleTime\":\"2022-08-01T01:00:00+0000\",\"sensitivityValue\":0.148,\"wave\":{\"sampleTime\":\"2022-08-01T01:00:00+0000\",\"deviceId\":\"Test Device Id 1\",\"channelNum\":1,\"sampleRateHz\":0.456,\"sensitivityUnits\":\"Hz\",\"tachSpeed\":12.1,\"samples\":[1.23,1.24,1.25,1.26]}},\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByCustomerSiteAreaAssetPointLocationPointsampleYearsampleMonthSampleTime(waveformdataVo.getCustomerAccount(), waveformdataVo.getSiteId().toString(), waveformdataVo.getAreaId().toString(), waveformdataVo.getAssetId().toString(), waveformdataVo.getPointLocationId().toString(), waveformdataVo.getPointId().toString(), ""+waveformdataVo.getSampleYear(), ""+waveformdataVo.getSampleMonth(), waveformdataVo.getSampleTime().toString());
        System.out.println("get expResult - " + expResult);
        System.out.println("get result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByCustomerSiteAreaAssetPointLocationPointApSetParamRow method, of class WaveDataController.
     */
    @Test
    public void test21_GetByCustomerSiteAreaAssetPointLocationPointsampleYearsampleMonthSampleTime() throws Exception {
        System.out.println("getByCustomerSiteAreaAssetPointLocationPointsampleYearsampleMonthSampleTime");
        String expResult = "{\"waveData\":{\"customerAccount\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"areaId\":\"98d2cfb9-069e-4dd7-8ac0-f2835351e5b0\",\"assetId\":\"b48a318e-352c-4c6e-a3e2-4ad7756681ce\",\"pointLocationId\":\"1485cbd3-28a7-4f21-80c4-f3ddb894465e\",\"pointId\":\"013c7746-16cf-4ae5-a124-87dd41c87f79\",\"sampleYear\":2022,\"sampleMonth\":8,\"sampleTime\":\"2022-11-16T03:00:00+0000\",\"sensitivityValue\":0.148,\"wave\":{\"sampleTime\":\"2022-11-16T03:00:00+0000\",\"deviceId\":\"Test Device Id 1\",\"channelNum\":1,\"sampleRateHz\":0.456,\"sensitivityUnits\":\"Hz\",\"tachSpeed\":12.1,\"samples\":[1.23,1.24,1.25,1.26]}},\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByCustomerSiteAreaAssetPointLocationPointsampleYearsampleMonthSampleTime(waveformdataVo.getCustomerAccount(), waveformdataVo.getSiteId().toString(), waveformdataVo.getAreaId().toString(), waveformdataVo.getAssetId().toString(), waveformdataVo.getPointLocationId().toString(), waveformdataVo.getPointId().toString(), ""+waveformdataVo.getSampleYear(), ""+waveformdataVo.getSampleMonth(), "2022-11-16T03:00:00Z");
        System.out.println("get expResult 21- " + expResult);
        System.out.println("get result 21   - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateWaveData method, of class WaveDataController.
     */
    @Test
    public void test3_UpdateWaveData() {
        System.out.println("updateWaveData");
        String content = "{\n" +
"    \"customerAccount\": \"77777\",\n" +
"    \"siteId\": \"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\n" +
"    \"areaId\": \"98d2cfb9-069e-4dd7-8ac0-f2835351e5b0\",\n" +
"    \"assetId\": \"b48a318e-352c-4c6e-a3e2-4ad7756681ce\",\n" +
"    \"pointLocationId\": \"1485cbd3-28a7-4f21-80c4-f3ddb894465e\",\n" +
"    \"pointId\": \"013c7746-16cf-4ae5-a124-87dd41c87f79\",\n" +
"    \"apSetId\": \"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\n" +
"    \"sampleYear\": \"2022\",\n" +
"    \"sampleMonth\": \"8\",\n" +
"    \"sampleTime\": \"2022-08-01T01:00:00Z\",\n" +
"    \"sensitivityValue\": 0.848,\n" +
"    \"waveform\": {\"sampleTime\" : \"2022-08-01T01:00:00Z\",\n" +
            "\"deviceId\" : \"Test Device Id 1\",\n" +
            "\"channelNum\" : \"1\",\n" +
            "\"sampleRateHz\" : \"0.456\",\n" +
            "\"sensitivityUnits\" : \"Hz\",\n" +
            "\"tachSpeed\" : \"12.1f\",\n" +
            "\"samples\" : [1.23, 1.24, 1.25,1.26]"
                + "}\n" +
"}";
        String expResult = "{\"outcome\":\"Updated WaveData Values successfully.\"}";
        String result = instance.updateWaveData(content);
        System.out.println("updateWaveData result - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of deleteWaveData method, of class WaveDataController.
     */
    @Test
    public void test4_DeleteWaveData() {
        System.out.println("deleteWaveData");
        String content = "{\n" +
"    \"customerAccount\": \"77777\",\n" +
"    \"siteId\": \"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\n" +
"    \"areaId\": \"98d2cfb9-069e-4dd7-8ac0-f2835351e5b0\",\n" +
"    \"assetId\": \"b48a318e-352c-4c6e-a3e2-4ad7756681ce\",\n" +
"    \"pointLocationId\": \"1485cbd3-28a7-4f21-80c4-f3ddb894465e\",\n" +
"    \"pointId\": \"013c7746-16cf-4ae5-a124-87dd41c87f79\",\n" +
"    \"apSetId\": \"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\n" +
"    \"sampleYear\": \"2022\",\n" +
"    \"sampleMonth\": \"8\",\n" +
"    \"sampleTime\": \"2022-08-01T01:00:00Z\",\n" +
"    \"sensitivityValue\": 0.148,\n" +
"    \"waveform\": {\"sampleTime\" : \"2022-08-01T01:00:00Z\",\n" +
            "\"deviceId\" : \"Test Device Id 1\",\n" +
            "\"channelNum\" : \"1\",\n" +
            "\"sampleRateHz\" : \"0.456\",\n" +
            "\"sensitivityUnits\" : \"Hz\",\n" +
            "\"tachSpeed\" : \"12.1f\",\n" +
            "\"samples\" : [1.23, 1.24, 1.25,1.26]"
                + "}\n" +
"}";
        String expResult = "{\"outcome\":\"Deleted WaveData Values successfully.\"}";
        String result = instance.deleteWaveData(content);
        System.out.println("deleteWaveData result - " + result);
        assertEquals(expResult, result);
    }

}
